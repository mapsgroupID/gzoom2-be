package it.mapsgroup.gzoom.jasper;



public interface ReportRunner {

    ReportHandler runReport(Report report);
}