package it.mapsgroup.gzoom.mybatis.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class UserLoginGZoom {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column USER_LOGIN.USER_LOGIN_ID
     *
     * @mbggenerated
     */
    private String userLoginId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column USER_LOGIN.CURRENT_PASSWORD
     *
     * @mbggenerated
     */
    private String currentPassword;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column USER_LOGIN.PASSWORD_HINT
     *
     * @mbggenerated
     */
    private String passwordHint;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column USER_LOGIN.IS_SYSTEM
     *
     * @mbggenerated
     */
    private Boolean isSystem;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column USER_LOGIN.ENABLED
     *
     * @mbggenerated
     */
    private Boolean enabled;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column USER_LOGIN.HAS_LOGGED_OUT
     *
     * @mbggenerated
     */
    private Boolean hasLoggedOut;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column USER_LOGIN.REQUIRE_PASSWORD_CHANGE
     *
     * @mbggenerated
     */
    private Boolean requirePasswordChange;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column USER_LOGIN.LAST_CURRENCY_UOM
     *
     * @mbggenerated
     */
    private String lastCurrencyUom;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column USER_LOGIN.LAST_LOCALE
     *
     * @mbggenerated
     */
    private String lastLocale;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column USER_LOGIN.LAST_TIME_ZONE
     *
     * @mbggenerated
     */
    private String lastTimeZone;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column USER_LOGIN.DISABLED_DATE_TIME
     *
     * @mbggenerated
     */
    private LocalDateTime disabledDateTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column USER_LOGIN.SUCCESSIVE_FAILED_LOGINS
     *
     * @mbggenerated
     */
    private BigDecimal successiveFailedLogins;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column USER_LOGIN.EXTERNAL_AUTH_ID
     *
     * @mbggenerated
     */
    private String externalAuthId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column USER_LOGIN.USER_LDAP_DN
     *
     * @mbggenerated
     */
    private String userLdapDn;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column USER_LOGIN.LAST_UPDATED_STAMP
     *
     * @mbggenerated
     */
    private LocalDateTime lastUpdatedStamp;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column USER_LOGIN.LAST_UPDATED_TX_STAMP
     *
     * @mbggenerated
     */
    private LocalDateTime lastUpdatedTxStamp;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column USER_LOGIN.CREATED_STAMP
     *
     * @mbggenerated
     */
    private LocalDateTime createdStamp;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column USER_LOGIN.CREATED_TX_STAMP
     *
     * @mbggenerated
     */
    private LocalDateTime createdTxStamp;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column USER_LOGIN.PARTY_ID
     *
     * @mbggenerated
     */
    private String partyId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column USER_LOGIN.EXTERNAL_SYSTEM
     *
     * @mbggenerated
     */
    private String externalSystem;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column USER_LOGIN.DESCRIPTION
     *
     * @mbggenerated
     */
    private String description;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column USER_LOGIN.USER_LOGIN_ID
     *
     * @return the value of USER_LOGIN.USER_LOGIN_ID
     *
     * @mbggenerated
     */
    public String getUserLoginId() {
        return userLoginId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column USER_LOGIN.USER_LOGIN_ID
     *
     * @param userLoginId the value for USER_LOGIN.USER_LOGIN_ID
     *
     * @mbggenerated
     */
    public void setUserLoginId(String userLoginId) {
        this.userLoginId = userLoginId == null ? null : userLoginId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column USER_LOGIN.CURRENT_PASSWORD
     *
     * @return the value of USER_LOGIN.CURRENT_PASSWORD
     *
     * @mbggenerated
     */
    public String getCurrentPassword() {
        return currentPassword;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column USER_LOGIN.CURRENT_PASSWORD
     *
     * @param currentPassword the value for USER_LOGIN.CURRENT_PASSWORD
     *
     * @mbggenerated
     */
    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword == null ? null : currentPassword.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column USER_LOGIN.PASSWORD_HINT
     *
     * @return the value of USER_LOGIN.PASSWORD_HINT
     *
     * @mbggenerated
     */
    public String getPasswordHint() {
        return passwordHint;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column USER_LOGIN.PASSWORD_HINT
     *
     * @param passwordHint the value for USER_LOGIN.PASSWORD_HINT
     *
     * @mbggenerated
     */
    public void setPasswordHint(String passwordHint) {
        this.passwordHint = passwordHint == null ? null : passwordHint.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column USER_LOGIN.IS_SYSTEM
     *
     * @return the value of USER_LOGIN.IS_SYSTEM
     *
     * @mbggenerated
     */
    public Boolean getIsSystem() {
        return isSystem;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column USER_LOGIN.IS_SYSTEM
     *
     * @param isSystem the value for USER_LOGIN.IS_SYSTEM
     *
     * @mbggenerated
     */
    public void setIsSystem(Boolean isSystem) {
        this.isSystem = isSystem;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column USER_LOGIN.ENABLED
     *
     * @return the value of USER_LOGIN.ENABLED
     *
     * @mbggenerated
     */
    public Boolean getEnabled() {
        return enabled;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column USER_LOGIN.ENABLED
     *
     * @param enabled the value for USER_LOGIN.ENABLED
     *
     * @mbggenerated
     */
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column USER_LOGIN.HAS_LOGGED_OUT
     *
     * @return the value of USER_LOGIN.HAS_LOGGED_OUT
     *
     * @mbggenerated
     */
    public Boolean getHasLoggedOut() {
        return hasLoggedOut;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column USER_LOGIN.HAS_LOGGED_OUT
     *
     * @param hasLoggedOut the value for USER_LOGIN.HAS_LOGGED_OUT
     *
     * @mbggenerated
     */
    public void setHasLoggedOut(Boolean hasLoggedOut) {
        this.hasLoggedOut = hasLoggedOut;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column USER_LOGIN.REQUIRE_PASSWORD_CHANGE
     *
     * @return the value of USER_LOGIN.REQUIRE_PASSWORD_CHANGE
     *
     * @mbggenerated
     */
    public Boolean getRequirePasswordChange() {
        return requirePasswordChange;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column USER_LOGIN.REQUIRE_PASSWORD_CHANGE
     *
     * @param requirePasswordChange the value for USER_LOGIN.REQUIRE_PASSWORD_CHANGE
     *
     * @mbggenerated
     */
    public void setRequirePasswordChange(Boolean requirePasswordChange) {
        this.requirePasswordChange = requirePasswordChange;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column USER_LOGIN.LAST_CURRENCY_UOM
     *
     * @return the value of USER_LOGIN.LAST_CURRENCY_UOM
     *
     * @mbggenerated
     */
    public String getLastCurrencyUom() {
        return lastCurrencyUom;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column USER_LOGIN.LAST_CURRENCY_UOM
     *
     * @param lastCurrencyUom the value for USER_LOGIN.LAST_CURRENCY_UOM
     *
     * @mbggenerated
     */
    public void setLastCurrencyUom(String lastCurrencyUom) {
        this.lastCurrencyUom = lastCurrencyUom == null ? null : lastCurrencyUom.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column USER_LOGIN.LAST_LOCALE
     *
     * @return the value of USER_LOGIN.LAST_LOCALE
     *
     * @mbggenerated
     */
    public String getLastLocale() {
        return lastLocale;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column USER_LOGIN.LAST_LOCALE
     *
     * @param lastLocale the value for USER_LOGIN.LAST_LOCALE
     *
     * @mbggenerated
     */
    public void setLastLocale(String lastLocale) {
        this.lastLocale = lastLocale == null ? null : lastLocale.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column USER_LOGIN.LAST_TIME_ZONE
     *
     * @return the value of USER_LOGIN.LAST_TIME_ZONE
     *
     * @mbggenerated
     */
    public String getLastTimeZone() {
        return lastTimeZone;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column USER_LOGIN.LAST_TIME_ZONE
     *
     * @param lastTimeZone the value for USER_LOGIN.LAST_TIME_ZONE
     *
     * @mbggenerated
     */
    public void setLastTimeZone(String lastTimeZone) {
        this.lastTimeZone = lastTimeZone == null ? null : lastTimeZone.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column USER_LOGIN.DISABLED_DATE_TIME
     *
     * @return the value of USER_LOGIN.DISABLED_DATE_TIME
     *
     * @mbggenerated
     */
    public LocalDateTime getDisabledDateTime() {
        return disabledDateTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column USER_LOGIN.DISABLED_DATE_TIME
     *
     * @param disabledDateTime the value for USER_LOGIN.DISABLED_DATE_TIME
     *
     * @mbggenerated
     */
    public void setDisabledDateTime(LocalDateTime disabledDateTime) {
        this.disabledDateTime = disabledDateTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column USER_LOGIN.SUCCESSIVE_FAILED_LOGINS
     *
     * @return the value of USER_LOGIN.SUCCESSIVE_FAILED_LOGINS
     *
     * @mbggenerated
     */
    public BigDecimal getSuccessiveFailedLogins() {
        return successiveFailedLogins;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column USER_LOGIN.SUCCESSIVE_FAILED_LOGINS
     *
     * @param successiveFailedLogins the value for USER_LOGIN.SUCCESSIVE_FAILED_LOGINS
     *
     * @mbggenerated
     */
    public void setSuccessiveFailedLogins(BigDecimal successiveFailedLogins) {
        this.successiveFailedLogins = successiveFailedLogins;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column USER_LOGIN.EXTERNAL_AUTH_ID
     *
     * @return the value of USER_LOGIN.EXTERNAL_AUTH_ID
     *
     * @mbggenerated
     */
    public String getExternalAuthId() {
        return externalAuthId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column USER_LOGIN.EXTERNAL_AUTH_ID
     *
     * @param externalAuthId the value for USER_LOGIN.EXTERNAL_AUTH_ID
     *
     * @mbggenerated
     */
    public void setExternalAuthId(String externalAuthId) {
        this.externalAuthId = externalAuthId == null ? null : externalAuthId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column USER_LOGIN.USER_LDAP_DN
     *
     * @return the value of USER_LOGIN.USER_LDAP_DN
     *
     * @mbggenerated
     */
    public String getUserLdapDn() {
        return userLdapDn;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column USER_LOGIN.USER_LDAP_DN
     *
     * @param userLdapDn the value for USER_LOGIN.USER_LDAP_DN
     *
     * @mbggenerated
     */
    public void setUserLdapDn(String userLdapDn) {
        this.userLdapDn = userLdapDn == null ? null : userLdapDn.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column USER_LOGIN.LAST_UPDATED_STAMP
     *
     * @return the value of USER_LOGIN.LAST_UPDATED_STAMP
     *
     * @mbggenerated
     */
    public LocalDateTime getLastUpdatedStamp() {
        return lastUpdatedStamp;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column USER_LOGIN.LAST_UPDATED_STAMP
     *
     * @param lastUpdatedStamp the value for USER_LOGIN.LAST_UPDATED_STAMP
     *
     * @mbggenerated
     */
    public void setLastUpdatedStamp(LocalDateTime lastUpdatedStamp) {
        this.lastUpdatedStamp = lastUpdatedStamp;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column USER_LOGIN.LAST_UPDATED_TX_STAMP
     *
     * @return the value of USER_LOGIN.LAST_UPDATED_TX_STAMP
     *
     * @mbggenerated
     */
    public LocalDateTime getLastUpdatedTxStamp() {
        return lastUpdatedTxStamp;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column USER_LOGIN.LAST_UPDATED_TX_STAMP
     *
     * @param lastUpdatedTxStamp the value for USER_LOGIN.LAST_UPDATED_TX_STAMP
     *
     * @mbggenerated
     */
    public void setLastUpdatedTxStamp(LocalDateTime lastUpdatedTxStamp) {
        this.lastUpdatedTxStamp = lastUpdatedTxStamp;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column USER_LOGIN.CREATED_STAMP
     *
     * @return the value of USER_LOGIN.CREATED_STAMP
     *
     * @mbggenerated
     */
    public LocalDateTime getCreatedStamp() {
        return createdStamp;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column USER_LOGIN.CREATED_STAMP
     *
     * @param createdStamp the value for USER_LOGIN.CREATED_STAMP
     *
     * @mbggenerated
     */
    public void setCreatedStamp(LocalDateTime createdStamp) {
        this.createdStamp = createdStamp;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column USER_LOGIN.CREATED_TX_STAMP
     *
     * @return the value of USER_LOGIN.CREATED_TX_STAMP
     *
     * @mbggenerated
     */
    public LocalDateTime getCreatedTxStamp() {
        return createdTxStamp;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column USER_LOGIN.CREATED_TX_STAMP
     *
     * @param createdTxStamp the value for USER_LOGIN.CREATED_TX_STAMP
     *
     * @mbggenerated
     */
    public void setCreatedTxStamp(LocalDateTime createdTxStamp) {
        this.createdTxStamp = createdTxStamp;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column USER_LOGIN.PARTY_ID
     *
     * @return the value of USER_LOGIN.PARTY_ID
     *
     * @mbggenerated
     */
    public String getPartyId() {
        return partyId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column USER_LOGIN.PARTY_ID
     *
     * @param partyId the value for USER_LOGIN.PARTY_ID
     *
     * @mbggenerated
     */
    public void setPartyId(String partyId) {
        this.partyId = partyId == null ? null : partyId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column USER_LOGIN.EXTERNAL_SYSTEM
     *
     * @return the value of USER_LOGIN.EXTERNAL_SYSTEM
     *
     * @mbggenerated
     */
    public String getExternalSystem() {
        return externalSystem;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column USER_LOGIN.EXTERNAL_SYSTEM
     *
     * @param externalSystem the value for USER_LOGIN.EXTERNAL_SYSTEM
     *
     * @mbggenerated
     */
    public void setExternalSystem(String externalSystem) {
        this.externalSystem = externalSystem == null ? null : externalSystem.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column USER_LOGIN.DESCRIPTION
     *
     * @return the value of USER_LOGIN.DESCRIPTION
     *
     * @mbggenerated
     */
    public String getDescription() {
        return description;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column USER_LOGIN.DESCRIPTION
     *
     * @param description the value for USER_LOGIN.DESCRIPTION
     *
     * @mbggenerated
     */
    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }
}