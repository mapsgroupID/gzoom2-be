package it.mapsgroup.gzoom.mybatis.dto;


import java.math.BigDecimal;

public class DetailKPI {

    private String workEffortId;
    private String workEffortName;
    private String workEffortNameLang;
    private String accountCode;
    private String accountName;
    private String accountNameLang;
    private String description;
    private String descriptionLang;
    private String abbreviation;
    private String abbreviationLang;
    private String scAmount;
    private String stAmount;
    private String sc2Amount;
    private String st2Amount;
    private String scIconAmount;
    private String drcObjectInfo;
    private String rvcIconContentId;
    private String stIconAmount;
    private String drtObjectInfo;
    private String rvtIconContentId;
    private String sc2IconAmount;
    private String drc2ObjectInfo;
    private String rvc2IconContentId;
    private String st2IconAmount;
    private String drt2ObjectInfo;
    private String rvt2IconContentId;
    private String glFiscalTypeId;
    private String descFTId;
    private BigDecimal decimalScale;
    private String m4Amount;
    private String m3Amount;
    private String m2Amount;
    private String m1Amount;
    private String isAmount;
    private String p1Amount;
    private String p2Amount;
    private String p3Amount;
    private String p4Amount;


    public BigDecimal getDecimalScale() { return decimalScale; }

    public void setDecimalScale(BigDecimal decimalScale) { this.decimalScale = decimalScale; }

    public String getWorkEffortId() {
        return workEffortId;
    }

    public void setWorkEffortId(String workEffortId) {
        this.workEffortId = workEffortId;
    }

    public String getWorkEffortName() {
        return workEffortName;
    }

    public void setWorkEffortName(String workEffortName) {
        this.workEffortName = workEffortName;
    }

    public String getWorkEffortNameLang() {
        return workEffortNameLang;
    }

    public void setWorkEffortNameLang(String workEffortNameLang) {
        this.workEffortNameLang = workEffortNameLang;
    }

    public String getAccountNameLang() {
        return accountNameLang;
    }

    public void setAccountNameLang(String accountNameLang) {
        this.accountNameLang = accountNameLang;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionLang() {
        return descriptionLang;
    }

    public void setDescriptionLang(String descriptionLang) {
        this.descriptionLang = descriptionLang;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getAbbreviationLang() {
        return abbreviationLang;
    }

    public void setAbbreviationLang(String abbreviationLang) {
        this.abbreviationLang = abbreviationLang;
    }

    public String getScAmount() {
        return scAmount;
    }

    public void setScAmount(String scAmount) {
        this.scAmount = scAmount;
    }

    public String getStAmount() {
        return stAmount;
    }

    public void setStAmount(String stAmount) {
        this.stAmount = stAmount;
    }

    public String getSc2Amount() {
        return sc2Amount;
    }

    public void setSc2Amount(String sc2Amount) {
        this.sc2Amount = sc2Amount;
    }

    public String getSt2Amount() {
        return st2Amount;
    }

    public void setSt2Amount(String st2Amount) {
        this.st2Amount = st2Amount;
    }

    public String getRvcIconContentId() {
        return rvcIconContentId;
    }

    public void setRvcIconContentId(String rvcIconContentId) {
        this.rvcIconContentId = rvcIconContentId;
    }

    public String getDrcObjectInfo() {
        return drcObjectInfo;
    }

    public void setDrcObjectInfo(String drcObjectInfo) {
        this.drcObjectInfo = drcObjectInfo;
    }

    public String getM4Amount() {
        return m4Amount;
    }

    public void setM4Amount(String m4Amount) {
        this.m4Amount = m4Amount;
    }

    public String getM3Amount() {
        return m3Amount;
    }

    public void setM3Amount(String m3Amount) {
        this.m3Amount = m3Amount;
    }

    public String getM2Amount() {
        return m2Amount;
    }

    public void setM2Amount(String m2Amount) {
        this.m2Amount = m2Amount;
    }

    public String getM1Amount() {
        return m1Amount;
    }

    public void setM1Amount(String m1Amount) {
        this.m1Amount = m1Amount;
    }

    public String getIsAmount() {
        return isAmount;
    }

    public void setIsAmount(String isAmount) {
        this.isAmount = isAmount;
    }

    public String getP1Amount() {
        return p1Amount;
    }

    public void setP1Amount(String p1Amount) {
        this.p1Amount = p1Amount;
    }

    public String getP2Amount() {
        return p2Amount;
    }

    public void setP2Amount(String p2Amount) {
        this.p2Amount = p2Amount;
    }

    public String getP3Amount() {
        return p3Amount;
    }

    public void setP3Amount(String p3Amount) {
        this.p3Amount = p3Amount;
    }

    public String getP4Amount() {
        return p4Amount;
    }

    public void setP4Amount(String p4Amount) {
        this.p4Amount = p4Amount;
    }

    public String getGlFiscalTypeId() {
        return glFiscalTypeId;
    }

    public void setGlFiscalTypeId(String glFiscalTypeId) {
        this.glFiscalTypeId = glFiscalTypeId;
    }

    public String getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(String accountCode) {
        this.accountCode = accountCode;
    }

    public String getDescFTId() {
        return descFTId;
    }

    public void setDescFTId(String descFTId) {
        this.descFTId = descFTId;
    }

    public String getScIconAmount() {
        return scIconAmount;
    }

    public void setScIconAmount(String scIconAmount) {
        this.scIconAmount = scIconAmount;
    }

    public String getStIconAmount() {
        return stIconAmount;
    }

    public void setStIconAmount(String stIconAmount) {
        this.stIconAmount = stIconAmount;
    }

    public String getDrtObjectInfo() {
        return drtObjectInfo;
    }

    public void setDrtObjectInfo(String drtObjectInfo) {
        this.drtObjectInfo = drtObjectInfo;
    }

    public String getRvtIconContentId() {
        return rvtIconContentId;
    }

    public void setRvtIconContentId(String rvtIconContentId) {
        this.rvtIconContentId = rvtIconContentId;
    }

    public String getSc2IconAmount() {
        return sc2IconAmount;
    }

    public void setSc2IconAmount(String sc2IconAmount) {
        this.sc2IconAmount = sc2IconAmount;
    }

    public String getDrc2ObjectInfo() {
        return drc2ObjectInfo;
    }

    public void setDrc2ObjectInfo(String drc2ObjectInfo) {
        this.drc2ObjectInfo = drc2ObjectInfo;
    }

    public String getRvc2IconContentId() {
        return rvc2IconContentId;
    }

    public void setRvc2IconContentId(String rvc2IconContentId) {
        this.rvc2IconContentId = rvc2IconContentId;
    }

    public String getSt2IconAmount() {
        return st2IconAmount;
    }

    public void setSt2IconAmount(String st2IconAmount) {
        this.st2IconAmount = st2IconAmount;
    }

    public String getDrt2ObjectInfo() {
        return drt2ObjectInfo;
    }

    public void setDrt2ObjectInfo(String drt2ObjectInfo) {
        this.drt2ObjectInfo = drt2ObjectInfo;
    }

    public String getRvt2IconContentId() {
        return rvt2IconContentId;
    }

    public void setRvt2IconContentId(String rvt2IconContentId) {
        this.rvt2IconContentId = rvt2IconContentId;
    }

}
