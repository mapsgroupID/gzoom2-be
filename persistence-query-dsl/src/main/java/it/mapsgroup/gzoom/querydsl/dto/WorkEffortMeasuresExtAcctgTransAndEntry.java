package it.mapsgroup.gzoom.querydsl.dto;

public class WorkEffortMeasuresExtAcctgTransAndEntry extends WorkEffortMeasure{

    private AcctgTransAndEntry acctgTransAndEntry;

    public AcctgTransAndEntry getAcctgTransAndEntry() {
        return acctgTransAndEntry;
    }

    public void setAcctgTransAndEntry(AcctgTransAndEntry acctgTransAndEntry) {
        this.acctgTransAndEntry = acctgTransAndEntry;
    }
}
