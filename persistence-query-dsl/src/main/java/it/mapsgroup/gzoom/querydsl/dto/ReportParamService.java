package it.mapsgroup.gzoom.querydsl.dto;

public class ReportParamService {
	private String serviceName;

	/**
	 * @return the serviceName
	 */
	public String getServiceName() {
		return serviceName;
	}

	/**
	 * @param serviceName the serviceName to set
	 */
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	
	
}
