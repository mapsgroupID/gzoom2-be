package it.mapsgroup.gzoom.querydsl.dto;

public class UserLoginValidPartyRoleEx extends UserLoginValidPartyRole {

    private PartyGroup partyGroup;

    public PartyGroup getPartyGroup() { return partyGroup;}

    public void setPartyGroup(PartyGroup partyGroup) { this.partyGroup = partyGroup;}


}
