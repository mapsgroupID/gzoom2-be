package it.mapsgroup.gzoom.querydsl.dto;

public class UomRatingScaleEx extends UomRatingScale {
    private Uom uom;

    public Uom getUom() {
        return uom;
    }

    public void setUom(Uom uom) {
        this.uom = uom;
    }
    
}
