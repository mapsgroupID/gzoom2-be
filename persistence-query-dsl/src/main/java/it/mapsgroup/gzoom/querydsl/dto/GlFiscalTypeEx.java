package it.mapsgroup.gzoom.querydsl.dto;

public class GlFiscalTypeEx extends GlFiscalType {

    private Enumeration enumeration;

    public Enumeration getEnumeration() {
        return enumeration;
    }

    public void setEnumeration(Enumeration enumeration) {
        this.enumeration = enumeration;
    }

}
