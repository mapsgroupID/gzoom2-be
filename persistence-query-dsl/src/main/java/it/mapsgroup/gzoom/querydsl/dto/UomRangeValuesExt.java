package it.mapsgroup.gzoom.querydsl.dto;

public class UomRangeValuesExt extends UomRangeValues {
    private DataResource dataResource;

    public DataResource getDataResource() {
        return dataResource;
    }

    public void setDataResource(DataResource dataResource) {
        this.dataResource = dataResource;
    }
}
