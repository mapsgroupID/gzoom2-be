package it.mapsgroup.gzoom.querydsl.dto;

public class TimeEntryEx extends TimeEntry {

    private WorkEffort workEffort;

    public WorkEffort getWorkEffort() {
        return workEffort;
    }

    public void setWorkEffort(WorkEffort workEffort) {
        this.workEffort = workEffort;
    }


}
