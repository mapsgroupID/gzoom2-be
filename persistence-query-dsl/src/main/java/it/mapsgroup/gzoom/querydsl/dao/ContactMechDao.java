package it.mapsgroup.gzoom.querydsl.dao;

import static org.slf4j.LoggerFactory.getLogger;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import com.querydsl.core.group.GroupBy;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.QBean;
import com.querydsl.sql.SQLBindings;
import com.querydsl.sql.SQLQuery;
import com.querydsl.sql.SQLQueryFactory;

import it.mapsgroup.gzoom.querydsl.dto.PartyContactMech;
import it.mapsgroup.gzoom.querydsl.dto.QContactMech;
import it.mapsgroup.gzoom.querydsl.dto.QPartyContactMech;
import it.mapsgroup.gzoom.querydsl.dto.QPartyRole;
import it.mapsgroup.gzoom.querydsl.dto.QWorkEffortTypeRole;

@Service
public class ContactMechDao extends AbstractDao {
	private static final Logger LOG = getLogger(ContactMechDao.class);

    private SQLQueryFactory queryFactory;
    
    @Autowired
    public ContactMechDao(SQLQueryFactory queryFactory) {
        this.queryFactory = queryFactory;
    }
    
    @Transactional
    public List<PartyContactMech> getContactMechWorkEffortTypeRole(String workEffortType) {
        if (TransactionSynchronizationManager.isActualTransactionActive()) {
            TransactionStatus status = TransactionAspectSupport.currentTransactionStatus();
            status.getClass();
        }
        
        QContactMech qContactMech = QContactMech.contactMech;
        QPartyRole qPartyRole = QPartyRole.partyRole;
        QPartyContactMech qPartyContactMech = QPartyContactMech.partyContactMech;
        QWorkEffortTypeRole qWorkEffortTypeRole = QWorkEffortTypeRole.workEffortTypeRole;

        SQLQuery<PartyContactMech> tupleSQLQuery = queryFactory.select(qPartyContactMech)
        		.from(qPartyRole)
        		.innerJoin(qPartyContactMech).on(qPartyContactMech.partyId.eq(qPartyRole.partyId)
        				.and(filterByDate(qPartyContactMech.fromDate, qPartyContactMech.thruDate)))
        		.innerJoin(qContactMech).on(qContactMech.contactMechId.eq(qPartyContactMech.contactMechId))
        		.innerJoin(qWorkEffortTypeRole).on(qWorkEffortTypeRole.roleTypeId.eq(qPartyRole.roleTypeId))
        		.where(qWorkEffortTypeRole.workEffortTypeId.eq(workEffortType)
        				.and(qContactMech.contactMechTypeId.eq("EMAIL_ADDRESS"))
        				.and(qContactMech.infoString.isNotNull()));

        SQLBindings bindings = tupleSQLQuery.getSQL();
        LOG.info("{}", bindings.getSQL());
        LOG.info("{}", bindings.getNullFriendlyBindings());
        QBean<PartyContactMech> list = Projections.bean(PartyContactMech.class, qPartyContactMech.all());
        List<PartyContactMech> ret = tupleSQLQuery.transform(GroupBy.groupBy(qPartyContactMech.partyId, qPartyContactMech.contactMechId).list(list));
        LOG.info("size = {}", ret.size());
        return ret;
    }
}
