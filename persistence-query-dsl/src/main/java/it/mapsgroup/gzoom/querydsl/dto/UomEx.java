package it.mapsgroup.gzoom.querydsl.dto;

public class UomEx extends Uom {
    private UomType uomType;

    public UomType getUomType() {
        return uomType;
    }

    public void setUomType(UomType uomType) {
        this.uomType = uomType;
    }
    
}
