package it.mapsgroup.gzoom.querydsl.dto;

import javax.annotation.Generated;
import com.querydsl.sql.Column;
import it.mapsgroup.gzoom.querydsl.AbstractIdentity;

/**
 * QueryConfig is a Querydsl bean type
 */
@Generated("com.querydsl.codegen.BeanSerializer")
public class QueryConfig implements AbstractIdentity {

    @Column("COND0_COMM")
    private String cond0Comm;

    @Column("COND0_INFO")
    private String cond0Info;

    @Column("COND0_NAME")
    private String cond0Name;

    @Column("COND1_COMM")
    private String cond1Comm;

    @Column("COND1_INFO")
    private String cond1Info;

    @Column("COND1_NAME")
    private String cond1Name;

    @Column("COND2_COMM")
    private String cond2Comm;

    @Column("COND2_INFO")
    private String cond2Info;

    @Column("COND2_NAME")
    private String cond2Name;

    @Column("COND3_COMM")
    private String cond3Comm;

    @Column("COND3_INFO")
    private String cond3Info;

    @Column("COND3_NAME")
    private String cond3Name;

    @Column("COND4_COMM")
    private String cond4Comm;

    @Column("COND4_INFO")
    private String cond4Info;

    @Column("COND4_NAME")
    private String cond4Name;

    @Column("COND5_COMM")
    private String cond5Comm;

    @Column("COND5_INFO")
    private String cond5Info;

    @Column("COND5_NAME")
    private String cond5Name;

    @Column("COND6_COMM")
    private String cond6Comm;

    @Column("COND6_INFO")
    private String cond6Info;

    @Column("COND6_NAME")
    private String cond6Name;

    @Column("COND7_COMM")
    private String cond7Comm;

    @Column("COND7_INFO")
    private String cond7Info;

    @Column("COND7_NAME")
    private String cond7Name;

    @Column("CREATED_STAMP")
    private java.time.LocalDateTime createdStamp;

    @Column("CREATED_TX_STAMP")
    private java.time.LocalDateTime createdTxStamp;

    @Column("LAST_UPDATED_STAMP")
    private java.time.LocalDateTime lastUpdatedStamp;

    @Column("LAST_UPDATED_TX_STAMP")
    private java.time.LocalDateTime lastUpdatedTxStamp;

    @Column("QUERY_ACTIVE")
    private Boolean queryActive;

    @Column("QUERY_CODE")
    private String queryCode;

    @Column("QUERY_COMM")
    private String queryComm;

    @Column("QUERY_CTX")
    private String queryCtx;

    @Column("QUERY_ID")
    private String queryId;

    @Column("QUERY_INFO")
    private String queryInfo;

    @Column("QUERY_NAME")
    private String queryName;

    @Column("QUERY_PUBLIC")
    private Boolean queryPublic;

    @Column("QUERY_TYPE")
    private String queryType;

    @Column("EXPORT_MIME_TYPE")
    private String exportMimeType;

    @Column("QUERY_COLUMNS_FORMAT_PARAM")
    private String queryColumnsFormaParam;

    public String getQueryColumnsFormaParam() {
        return queryColumnsFormaParam;
    }

    public void setQueryColumnsFormaParam(String queryColumnsFormaParam) {
        this.queryColumnsFormaParam = queryColumnsFormaParam;
    }

    public String getCond0Comm() {
        return cond0Comm;
    }

    public void setCond0Comm(String cond0Comm) {
        this.cond0Comm = cond0Comm;
    }

    public String getCond0Info() {
        return cond0Info;
    }

    public void setCond0Info(String cond0Info) {
        this.cond0Info = cond0Info;
    }

    public String getCond0Name() {
        return cond0Name;
    }

    public void setCond0Name(String cond0Name) {
        this.cond0Name = cond0Name;
    }

    public String getCond1Comm() {
        return cond1Comm;
    }

    public void setCond1Comm(String cond1Comm) {
        this.cond1Comm = cond1Comm;
    }

    public String getCond1Info() {
        return cond1Info;
    }

    public void setCond1Info(String cond1Info) {
        this.cond1Info = cond1Info;
    }

    public String getCond1Name() {
        return cond1Name;
    }

    public void setCond1Name(String cond1Name) {
        this.cond1Name = cond1Name;
    }

    public String getCond2Comm() {
        return cond2Comm;
    }

    public void setCond2Comm(String cond2Comm) {
        this.cond2Comm = cond2Comm;
    }

    public String getCond2Info() {
        return cond2Info;
    }

    public void setCond2Info(String cond2Info) {
        this.cond2Info = cond2Info;
    }

    public String getCond2Name() {
        return cond2Name;
    }

    public void setCond2Name(String cond2Name) {
        this.cond2Name = cond2Name;
    }

    public String getCond3Comm() {
        return cond3Comm;
    }

    public void setCond3Comm(String cond3Comm) {
        this.cond3Comm = cond3Comm;
    }

    public String getCond3Info() {
        return cond3Info;
    }

    public void setCond3Info(String cond3Info) {
        this.cond3Info = cond3Info;
    }

    public String getCond3Name() {
        return cond3Name;
    }

    public void setCond3Name(String cond3Name) {
        this.cond3Name = cond3Name;
    }

    public String getCond4Comm() {
        return cond4Comm;
    }

    public void setCond4Comm(String cond4Comm) {
        this.cond4Comm = cond4Comm;
    }

    public String getCond4Info() {
        return cond4Info;
    }

    public void setCond4Info(String cond4Info) {
        this.cond4Info = cond4Info;
    }

    public String getCond4Name() {
        return cond4Name;
    }

    public void setCond4Name(String cond4Name) {
        this.cond4Name = cond4Name;
    }

    public String getCond5Comm() {
        return cond5Comm;
    }

    public void setCond5Comm(String cond5Comm) {
        this.cond5Comm = cond5Comm;
    }

    public String getCond5Info() {
        return cond5Info;
    }

    public void setCond5Info(String cond5Info) {
        this.cond5Info = cond5Info;
    }

    public String getCond5Name() {
        return cond5Name;
    }

    public void setCond5Name(String cond5Name) {
        this.cond5Name = cond5Name;
    }

    public String getCond6Comm() {
        return cond6Comm;
    }

    public void setCond6Comm(String cond6Comm) {
        this.cond6Comm = cond6Comm;
    }

    public String getCond6Info() {
        return cond6Info;
    }

    public void setCond6Info(String cond6Info) {
        this.cond6Info = cond6Info;
    }

    public String getCond6Name() {
        return cond6Name;
    }

    public void setCond6Name(String cond6Name) {
        this.cond6Name = cond6Name;
    }

    public String getCond7Comm() {
        return cond7Comm;
    }

    public void setCond7Comm(String cond7Comm) {
        this.cond7Comm = cond7Comm;
    }

    public String getCond7Info() {
        return cond7Info;
    }

    public void setCond7Info(String cond7Info) {
        this.cond7Info = cond7Info;
    }

    public String getCond7Name() {
        return cond7Name;
    }

    public void setCond7Name(String cond7Name) {
        this.cond7Name = cond7Name;
    }

    public java.time.LocalDateTime getCreatedStamp() {
        return createdStamp;
    }

    public void setCreatedStamp(java.time.LocalDateTime createdStamp) {
        this.createdStamp = createdStamp;
    }

    public java.time.LocalDateTime getCreatedTxStamp() {
        return createdTxStamp;
    }

    public void setCreatedTxStamp(java.time.LocalDateTime createdTxStamp) {
        this.createdTxStamp = createdTxStamp;
    }

    public java.time.LocalDateTime getLastUpdatedStamp() {
        return lastUpdatedStamp;
    }

    public void setLastUpdatedStamp(java.time.LocalDateTime lastUpdatedStamp) {
        this.lastUpdatedStamp = lastUpdatedStamp;
    }

    public java.time.LocalDateTime getLastUpdatedTxStamp() {
        return lastUpdatedTxStamp;
    }

    public void setLastUpdatedTxStamp(java.time.LocalDateTime lastUpdatedTxStamp) {
        this.lastUpdatedTxStamp = lastUpdatedTxStamp;
    }

    public Boolean getQueryActive() {
        return queryActive;
    }

    public void setQueryActive(Boolean queryActive) {
        this.queryActive = queryActive;
    }

    public String getQueryCode() {
        return queryCode;
    }

    public void setQueryCode(String queryCode) {
        this.queryCode = queryCode;
    }

    public String getQueryComm() {
        return queryComm;
    }

    public void setQueryComm(String queryComm) {
        this.queryComm = queryComm;
    }

    public String getQueryCtx() {
        return queryCtx;
    }

    public void setQueryCtx(String queryCtx) {
        this.queryCtx = queryCtx;
    }

    public String getQueryId() {
        return queryId;
    }

    public void setQueryId(String queryId) {
        this.queryId = queryId;
    }

    public String getQueryInfo() {
        return queryInfo;
    }

    public void setQueryInfo(String queryInfo) {
        this.queryInfo = queryInfo;
    }

    public String getQueryName() {
        return queryName;
    }

    public void setQueryName(String queryName) {
        this.queryName = queryName;
    }

    public Boolean getQueryPublic() {
        return queryPublic;
    }

    public void setQueryPublic(Boolean queryPublic) {
        this.queryPublic = queryPublic;
    }

    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    public String getExportMimeType() {
        return exportMimeType;
    }

    public void setExportMimeType(String exportMimeType) {
        this.exportMimeType = exportMimeType;
    }


}

