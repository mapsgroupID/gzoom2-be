package it.mapsgroup.gzoom.querydsl.dto;

import javax.annotation.Generated;
import com.querydsl.sql.Column;

/**
 * GlAccountInterfaceExt is a Querydsl bean type
 */
@Generated("com.querydsl.codegen.BeanSerializer")
public class GlAccountInterfaceExt {

    @Column("ACCOUNT_CODE")
    private String accountCode;

    @Column("ACCOUNT_NAME")
    private String accountName;

    @Column("ACCOUNT_NAME_LANG")
    private String accountNameLang;

    @Column("ACCOUNT_TYPE_ENUM_ID")
    private String accountTypeEnumId;

    @Column("ACCOUNT_TYPE_ID")
    private String accountTypeId;

    @Column("CALC_CUSTOM_METHOD_ID")
    private String calcCustomMethodId;

    @Column("CUSTOM_DATE01")
    private java.time.LocalDateTime customDate01;

    @Column("CUSTOM_DATE02")
    private java.time.LocalDateTime customDate02;

    @Column("CUSTOM_DATE03")
    private java.time.LocalDateTime customDate03;

    @Column("CUSTOM_DATE04")
    private java.time.LocalDateTime customDate04;

    @Column("CUSTOM_DATE05")
    private java.time.LocalDateTime customDate05;

    @Column("CUSTOM_TEXT01")
    private String customText01;

    @Column("CUSTOM_TEXT02")
    private String customText02;

    @Column("CUSTOM_TEXT03")
    private String customText03;

    @Column("CUSTOM_TEXT04")
    private String customText04;

    @Column("CUSTOM_TEXT05")
    private String customText05;

    @Column("CUSTOM_VALUE01")
    private java.math.BigDecimal customValue01;

    @Column("CUSTOM_VALUE02")
    private java.math.BigDecimal customValue02;

    @Column("CUSTOM_VALUE03")
    private java.math.BigDecimal customValue03;

    @Column("CUSTOM_VALUE04")
    private java.math.BigDecimal customValue04;

    @Column("CUSTOM_VALUE05")
    private java.math.BigDecimal customValue05;

    @Column("DATA_SOURCE")
    private String dataSource;

    @Column("DEBIT_CREDIT_DEFAULT")
    private Boolean debitCreditDefault;

    @Column("DEFAULT_UOM_CODE")
    private String defaultUomCode;

    @Column("DESCRIPTION")
    private String description;

    @Column("DESCRIPTION_LANG")
    private String descriptionLang;

    @Column("DETECT_ORG_UNIT_ID_FLAG")
    private Boolean detectOrgUnitIdFlag;

    @Column("FROM_DATE")
    private java.time.LocalDateTime fromDate;

    @Column("GL_ACCOUNT_CLASS_CODE")
    private String glAccountClassCode;

    @Column("GL_RESOURCE_TYPE_ID")
    private String glResourceTypeId;

    @Column("ID")
    private String id;

    @Column("INPUT_ENUM_ID")
    private String inputEnumId;

    @Column("PARTY_ID_CDC")
    private String partyIdCdc;

    @Column("PARTY_ID_CDR")
    private String partyIdCdr;

    @Column("PERIOD_TYPE_DESC")
    private String periodTypeDesc;

    @Column("PRIO_CALC")
    private java.math.BigInteger prioCalc;

    @Column("PRODUCT_ID")
    private String productId;

    @Column("PURPOSE_TYPE_ID")
    private String purposeTypeId;

    @Column("REF_DATE")
    private java.time.LocalDateTime refDate;

    @Column("REFERENCED_ACCOUNT_CODE")
    private String referencedAccountCode;

    @Column("ROLE_TYPE_ID_CDC")
    private String roleTypeIdCdc;

    @Column("ROLE_TYPE_ID_CDR")
    private String roleTypeIdCdr;

    @Column("SEQ")
    private java.math.BigInteger seq;

    @Column("SOURCE")
    private String source;

    @Column("SOURCE_LANG")
    private String sourceLang;

    @Column("STATO")
    private String stato;

    @Column("THRU_DATE")
    private java.time.LocalDateTime thruDate;

    @Column("UOM_RANGE_ID")
    private String uomRangeId;

    @Column("WE_MEASURE_TYPE_ENUM_ID")
    private String weMeasureTypeEnumId;

    public String getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(String accountCode) {
        this.accountCode = accountCode;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAccountNameLang() {
        return accountNameLang;
    }

    public void setAccountNameLang(String accountNameLang) {
        this.accountNameLang = accountNameLang;
    }

    public String getAccountTypeEnumId() {
        return accountTypeEnumId;
    }

    public void setAccountTypeEnumId(String accountTypeEnumId) {
        this.accountTypeEnumId = accountTypeEnumId;
    }

    public String getAccountTypeId() {
        return accountTypeId;
    }

    public void setAccountTypeId(String accountTypeId) {
        this.accountTypeId = accountTypeId;
    }

    public String getCalcCustomMethodId() {
        return calcCustomMethodId;
    }

    public void setCalcCustomMethodId(String calcCustomMethodId) {
        this.calcCustomMethodId = calcCustomMethodId;
    }

    public java.time.LocalDateTime getCustomDate01() {
        return customDate01;
    }

    public void setCustomDate01(java.time.LocalDateTime customDate01) {
        this.customDate01 = customDate01;
    }

    public java.time.LocalDateTime getCustomDate02() {
        return customDate02;
    }

    public void setCustomDate02(java.time.LocalDateTime customDate02) {
        this.customDate02 = customDate02;
    }

    public java.time.LocalDateTime getCustomDate03() {
        return customDate03;
    }

    public void setCustomDate03(java.time.LocalDateTime customDate03) {
        this.customDate03 = customDate03;
    }

    public java.time.LocalDateTime getCustomDate04() {
        return customDate04;
    }

    public void setCustomDate04(java.time.LocalDateTime customDate04) {
        this.customDate04 = customDate04;
    }

    public java.time.LocalDateTime getCustomDate05() {
        return customDate05;
    }

    public void setCustomDate05(java.time.LocalDateTime customDate05) {
        this.customDate05 = customDate05;
    }

    public String getCustomText01() {
        return customText01;
    }

    public void setCustomText01(String customText01) {
        this.customText01 = customText01;
    }

    public String getCustomText02() {
        return customText02;
    }

    public void setCustomText02(String customText02) {
        this.customText02 = customText02;
    }

    public String getCustomText03() {
        return customText03;
    }

    public void setCustomText03(String customText03) {
        this.customText03 = customText03;
    }

    public String getCustomText04() {
        return customText04;
    }

    public void setCustomText04(String customText04) {
        this.customText04 = customText04;
    }

    public String getCustomText05() {
        return customText05;
    }

    public void setCustomText05(String customText05) {
        this.customText05 = customText05;
    }

    public java.math.BigDecimal getCustomValue01() {
        return customValue01;
    }

    public void setCustomValue01(java.math.BigDecimal customValue01) {
        this.customValue01 = customValue01;
    }

    public java.math.BigDecimal getCustomValue02() {
        return customValue02;
    }

    public void setCustomValue02(java.math.BigDecimal customValue02) {
        this.customValue02 = customValue02;
    }

    public java.math.BigDecimal getCustomValue03() {
        return customValue03;
    }

    public void setCustomValue03(java.math.BigDecimal customValue03) {
        this.customValue03 = customValue03;
    }

    public java.math.BigDecimal getCustomValue04() {
        return customValue04;
    }

    public void setCustomValue04(java.math.BigDecimal customValue04) {
        this.customValue04 = customValue04;
    }

    public java.math.BigDecimal getCustomValue05() {
        return customValue05;
    }

    public void setCustomValue05(java.math.BigDecimal customValue05) {
        this.customValue05 = customValue05;
    }

    public String getDataSource() {
        return dataSource;
    }

    public void setDataSource(String dataSource) {
        this.dataSource = dataSource;
    }

    public Boolean getDebitCreditDefault() {
        return debitCreditDefault;
    }

    public void setDebitCreditDefault(Boolean debitCreditDefault) {
        this.debitCreditDefault = debitCreditDefault;
    }

    public String getDefaultUomCode() {
        return defaultUomCode;
    }

    public void setDefaultUomCode(String defaultUomCode) {
        this.defaultUomCode = defaultUomCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionLang() {
        return descriptionLang;
    }

    public void setDescriptionLang(String descriptionLang) {
        this.descriptionLang = descriptionLang;
    }

    public Boolean getDetectOrgUnitIdFlag() {
        return detectOrgUnitIdFlag;
    }

    public void setDetectOrgUnitIdFlag(Boolean detectOrgUnitIdFlag) {
        this.detectOrgUnitIdFlag = detectOrgUnitIdFlag;
    }

    public java.time.LocalDateTime getFromDate() {
        return fromDate;
    }

    public void setFromDate(java.time.LocalDateTime fromDate) {
        this.fromDate = fromDate;
    }

    public String getGlAccountClassCode() {
        return glAccountClassCode;
    }

    public void setGlAccountClassCode(String glAccountClassCode) {
        this.glAccountClassCode = glAccountClassCode;
    }

    public String getGlResourceTypeId() {
        return glResourceTypeId;
    }

    public void setGlResourceTypeId(String glResourceTypeId) {
        this.glResourceTypeId = glResourceTypeId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInputEnumId() {
        return inputEnumId;
    }

    public void setInputEnumId(String inputEnumId) {
        this.inputEnumId = inputEnumId;
    }

    public String getPartyIdCdc() {
        return partyIdCdc;
    }

    public void setPartyIdCdc(String partyIdCdc) {
        this.partyIdCdc = partyIdCdc;
    }

    public String getPartyIdCdr() {
        return partyIdCdr;
    }

    public void setPartyIdCdr(String partyIdCdr) {
        this.partyIdCdr = partyIdCdr;
    }

    public String getPeriodTypeDesc() {
        return periodTypeDesc;
    }

    public void setPeriodTypeDesc(String periodTypeDesc) {
        this.periodTypeDesc = periodTypeDesc;
    }

    public java.math.BigInteger getPrioCalc() {
        return prioCalc;
    }

    public void setPrioCalc(java.math.BigInteger prioCalc) {
        this.prioCalc = prioCalc;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getPurposeTypeId() {
        return purposeTypeId;
    }

    public void setPurposeTypeId(String purposeTypeId) {
        this.purposeTypeId = purposeTypeId;
    }

    public java.time.LocalDateTime getRefDate() {
        return refDate;
    }

    public void setRefDate(java.time.LocalDateTime refDate) {
        this.refDate = refDate;
    }

    public String getReferencedAccountCode() {
        return referencedAccountCode;
    }

    public void setReferencedAccountCode(String referencedAccountCode) {
        this.referencedAccountCode = referencedAccountCode;
    }

    public String getRoleTypeIdCdc() {
        return roleTypeIdCdc;
    }

    public void setRoleTypeIdCdc(String roleTypeIdCdc) {
        this.roleTypeIdCdc = roleTypeIdCdc;
    }

    public String getRoleTypeIdCdr() {
        return roleTypeIdCdr;
    }

    public void setRoleTypeIdCdr(String roleTypeIdCdr) {
        this.roleTypeIdCdr = roleTypeIdCdr;
    }

    public java.math.BigInteger getSeq() {
        return seq;
    }

    public void setSeq(java.math.BigInteger seq) {
        this.seq = seq;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSourceLang() {
        return sourceLang;
    }

    public void setSourceLang(String sourceLang) {
        this.sourceLang = sourceLang;
    }

    public String getStato() {
        return stato;
    }

    public void setStato(String stato) {
        this.stato = stato;
    }

    public java.time.LocalDateTime getThruDate() {
        return thruDate;
    }

    public void setThruDate(java.time.LocalDateTime thruDate) {
        this.thruDate = thruDate;
    }

    public String getUomRangeId() {
        return uomRangeId;
    }

    public void setUomRangeId(String uomRangeId) {
        this.uomRangeId = uomRangeId;
    }

    public String getWeMeasureTypeEnumId() {
        return weMeasureTypeEnumId;
    }

    public void setWeMeasureTypeEnumId(String weMeasureTypeEnumId) {
        this.weMeasureTypeEnumId = weMeasureTypeEnumId;
    }

}

