package it.mapsgroup.gzoom.querydsl.dto;

import javax.annotation.Generated;
import com.querydsl.sql.Column;

/**
 * OrganizationInterface is a Querydsl bean type
 */
@Generated("com.querydsl.codegen.BeanSerializer")
public class OrganizationInterface {

    @Column("CUSTOM_DATE01")
    private java.time.LocalDateTime customDate01;

    @Column("CUSTOM_DATE02")
    private java.time.LocalDateTime customDate02;

    @Column("CUSTOM_DATE03")
    private java.time.LocalDateTime customDate03;

    @Column("CUSTOM_DATE04")
    private java.time.LocalDateTime customDate04;

    @Column("CUSTOM_DATE05")
    private java.time.LocalDateTime customDate05;

    @Column("CUSTOM_TEXT01")
    private String customText01;

    @Column("CUSTOM_TEXT02")
    private String customText02;

    @Column("CUSTOM_TEXT03")
    private String customText03;

    @Column("CUSTOM_TEXT04")
    private String customText04;

    @Column("CUSTOM_TEXT05")
    private String customText05;

    @Column("CUSTOM_VALUE01")
    private java.math.BigDecimal customValue01;

    @Column("CUSTOM_VALUE02")
    private java.math.BigDecimal customValue02;

    @Column("CUSTOM_VALUE03")
    private java.math.BigDecimal customValue03;

    @Column("CUSTOM_VALUE04")
    private java.math.BigDecimal customValue04;

    @Column("CUSTOM_VALUE05")
    private java.math.BigDecimal customValue05;

    @Column("DATA_SOURCE")
    private String dataSource;

    @Column("DESCRIPTION")
    private String description;

    @Column("DESCRIPTION_LANG")
    private String descriptionLang;

    @Column("ID")
    private String id;

    @Column("LONG_DESCRIPTION")
    private String longDescription;

    @Column("LONG_DESCRIPTION_LANG")
    private String longDescriptionLang;

    @Column("ORG_CODE")
    private String orgCode;

    @Column("ORG_ROLE_TYPE_ID")
    private String orgRoleTypeId;

    @Column("PARENT_FROM_DATE")
    private java.time.LocalDateTime parentFromDate;

    @Column("PARENT_ORG_CODE")
    private String parentOrgCode;

    @Column("PARENT_ROLE_TYPE_ID")
    private String parentRoleTypeId;

    @Column("REF_DATE")
    private java.time.LocalDateTime refDate;

    @Column("RESPONSIBLE_CODE")
    private String responsibleCode;

    @Column("RESPONSIBLE_COMMENTS")
    private String responsibleComments;

    @Column("RESPONSIBLE_FROM_DATE")
    private java.time.LocalDateTime responsibleFromDate;

    @Column("RESPONSIBLE_ROLE_TYPE_ID")
    private String responsibleRoleTypeId;

    @Column("RESPONSIBLE_THRU_DATE")
    private java.time.LocalDateTime responsibleThruDate;

    @Column("SEQ")
    private java.math.BigInteger seq;

    @Column("STATO")
    private String stato;

    @Column("THRU_DATE")
    private java.time.LocalDateTime thruDate;

    @Column("VAT_CODE")
    private String vatCode;

    public java.time.LocalDateTime getCustomDate01() {
        return customDate01;
    }

    public void setCustomDate01(java.time.LocalDateTime customDate01) {
        this.customDate01 = customDate01;
    }

    public java.time.LocalDateTime getCustomDate02() {
        return customDate02;
    }

    public void setCustomDate02(java.time.LocalDateTime customDate02) {
        this.customDate02 = customDate02;
    }

    public java.time.LocalDateTime getCustomDate03() {
        return customDate03;
    }

    public void setCustomDate03(java.time.LocalDateTime customDate03) {
        this.customDate03 = customDate03;
    }

    public java.time.LocalDateTime getCustomDate04() {
        return customDate04;
    }

    public void setCustomDate04(java.time.LocalDateTime customDate04) {
        this.customDate04 = customDate04;
    }

    public java.time.LocalDateTime getCustomDate05() {
        return customDate05;
    }

    public void setCustomDate05(java.time.LocalDateTime customDate05) {
        this.customDate05 = customDate05;
    }

    public String getCustomText01() {
        return customText01;
    }

    public void setCustomText01(String customText01) {
        this.customText01 = customText01;
    }

    public String getCustomText02() {
        return customText02;
    }

    public void setCustomText02(String customText02) {
        this.customText02 = customText02;
    }

    public String getCustomText03() {
        return customText03;
    }

    public void setCustomText03(String customText03) {
        this.customText03 = customText03;
    }

    public String getCustomText04() {
        return customText04;
    }

    public void setCustomText04(String customText04) {
        this.customText04 = customText04;
    }

    public String getCustomText05() {
        return customText05;
    }

    public void setCustomText05(String customText05) {
        this.customText05 = customText05;
    }

    public java.math.BigDecimal getCustomValue01() {
        return customValue01;
    }

    public void setCustomValue01(java.math.BigDecimal customValue01) {
        this.customValue01 = customValue01;
    }

    public java.math.BigDecimal getCustomValue02() {
        return customValue02;
    }

    public void setCustomValue02(java.math.BigDecimal customValue02) {
        this.customValue02 = customValue02;
    }

    public java.math.BigDecimal getCustomValue03() {
        return customValue03;
    }

    public void setCustomValue03(java.math.BigDecimal customValue03) {
        this.customValue03 = customValue03;
    }

    public java.math.BigDecimal getCustomValue04() {
        return customValue04;
    }

    public void setCustomValue04(java.math.BigDecimal customValue04) {
        this.customValue04 = customValue04;
    }

    public java.math.BigDecimal getCustomValue05() {
        return customValue05;
    }

    public void setCustomValue05(java.math.BigDecimal customValue05) {
        this.customValue05 = customValue05;
    }

    public String getDataSource() {
        return dataSource;
    }

    public void setDataSource(String dataSource) {
        this.dataSource = dataSource;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionLang() {
        return descriptionLang;
    }

    public void setDescriptionLang(String descriptionLang) {
        this.descriptionLang = descriptionLang;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    public String getLongDescriptionLang() {
        return longDescriptionLang;
    }

    public void setLongDescriptionLang(String longDescriptionLang) {
        this.longDescriptionLang = longDescriptionLang;
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public String getOrgRoleTypeId() {
        return orgRoleTypeId;
    }

    public void setOrgRoleTypeId(String orgRoleTypeId) {
        this.orgRoleTypeId = orgRoleTypeId;
    }

    public java.time.LocalDateTime getParentFromDate() {
        return parentFromDate;
    }

    public void setParentFromDate(java.time.LocalDateTime parentFromDate) {
        this.parentFromDate = parentFromDate;
    }

    public String getParentOrgCode() {
        return parentOrgCode;
    }

    public void setParentOrgCode(String parentOrgCode) {
        this.parentOrgCode = parentOrgCode;
    }

    public String getParentRoleTypeId() {
        return parentRoleTypeId;
    }

    public void setParentRoleTypeId(String parentRoleTypeId) {
        this.parentRoleTypeId = parentRoleTypeId;
    }

    public java.time.LocalDateTime getRefDate() {
        return refDate;
    }

    public void setRefDate(java.time.LocalDateTime refDate) {
        this.refDate = refDate;
    }

    public String getResponsibleCode() {
        return responsibleCode;
    }

    public void setResponsibleCode(String responsibleCode) {
        this.responsibleCode = responsibleCode;
    }

    public String getResponsibleComments() {
        return responsibleComments;
    }

    public void setResponsibleComments(String responsibleComments) {
        this.responsibleComments = responsibleComments;
    }

    public java.time.LocalDateTime getResponsibleFromDate() {
        return responsibleFromDate;
    }

    public void setResponsibleFromDate(java.time.LocalDateTime responsibleFromDate) {
        this.responsibleFromDate = responsibleFromDate;
    }

    public String getResponsibleRoleTypeId() {
        return responsibleRoleTypeId;
    }

    public void setResponsibleRoleTypeId(String responsibleRoleTypeId) {
        this.responsibleRoleTypeId = responsibleRoleTypeId;
    }

    public java.time.LocalDateTime getResponsibleThruDate() {
        return responsibleThruDate;
    }

    public void setResponsibleThruDate(java.time.LocalDateTime responsibleThruDate) {
        this.responsibleThruDate = responsibleThruDate;
    }

    public java.math.BigInteger getSeq() {
        return seq;
    }

    public void setSeq(java.math.BigInteger seq) {
        this.seq = seq;
    }

    public String getStato() {
        return stato;
    }

    public void setStato(String stato) {
        this.stato = stato;
    }

    public java.time.LocalDateTime getThruDate() {
        return thruDate;
    }

    public void setThruDate(java.time.LocalDateTime thruDate) {
        this.thruDate = thruDate;
    }

    public String getVatCode() {
        return vatCode;
    }

    public void setVatCode(String vatCode) {
        this.vatCode = vatCode;
    }

}

