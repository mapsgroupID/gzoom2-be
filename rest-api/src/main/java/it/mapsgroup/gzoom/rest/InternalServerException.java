package it.mapsgroup.gzoom.rest;

public class InternalServerException extends RuntimeException {
	private static final long serialVersionUID = 8154245311865662753L;

	public InternalServerException(String message) {
		super(message);
	}

}
