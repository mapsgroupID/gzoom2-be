package it.mapsgroup.gzoom.model;

public class EquipmentEmployment extends AbstractEmployment {

	private Equipment equipment;

	public Equipment getEquipment() {
		return equipment;
	}

	public void setEquipment(Equipment equipment) {
		this.equipment = equipment;
	}
}
