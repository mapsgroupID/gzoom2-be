package it.mapsgroup.gzoom.model;

/**
 * @author Andrea Fossi.
 */
public enum SecondmentFilterEnum {
    NONE,
    SECONDMENT,
    NOT_SECONDMENT

}
