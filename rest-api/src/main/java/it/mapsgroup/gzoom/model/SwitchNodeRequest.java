package it.mapsgroup.gzoom.model;

/**
 * @author Andrea Fossi.
 */
public class SwitchNodeRequest {
    private Long nodeId;

    public Long getNodeId() {
        return nodeId;
    }

    public void setNodeId(Long nodeId) {
        this.nodeId = nodeId;
    }
}
