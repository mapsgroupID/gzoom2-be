package it.mapsgroup.gzoom.model;

/**
 * @author Andrea Fossi.
 */
public enum WorkLogEventType {
    PERSON, EQUIPMENT
}
