package it.mapsgroup.gzoom.model;

/**
 * @author Andrea Fossi.
 */
public class WeeklyWorkLogCompanyEquipment {
    private Long id;
    private String registrationNumber;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }
}
