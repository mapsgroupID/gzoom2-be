package it.mapsgroup.gzoom.model;

public class WeeklyWorkLogEquipmentChange extends Identity {

    private String registrationNumber;

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }
}
