package it.mapsgroup.gzoom.model;

/**
 * @author Fabio G. Strozzi
 */
public class Messages {
    public static final String INVALID_UOM_TYPE = "Invalid uom type";
    public static final String UOM_TYPE_REQUIRED = "An uom type is required";
    public static final String UOM_TYPE_ID_REQUIRED = "Uom type id is required";
    public static final String UOM_TYPE_DESCRIPTION_REQUIRED = "Uom type description is required";
    
    public static final String INVALID_UOM = "Invalid uom";
    public static final String UOM_REQUIRED = "An uom is required";
    public static final String UOM_ID_REQUIRED = "Uom id is required";
    public static final String UOM_DESCRIPTION_REQUIRED = "Uom description is required";
    public static final String UOM_ABBREVIATION_REQUIRED = "Uom abbreviation is required";
    public static final String UOM_MIN_VALUE_GREATER_THAN_MAX_VALUE = "Min Value is greater than Max Value";
    public static final String UOM_MIN_VALUE_AND_MAX_VALUE = "Insert both Min Value and Max Value";
    
    public static final String INVALID_UOM_RATING_SCALE = "Invalid uomRatingScale";
    public static final String UOM_RATING_SCALE_REQUIRED = "An uomRatingScale is required";
    public static final String UOM_RATING_SCALE_DESCRIPTION_REQUIRED = "UomRatingScale description is required";
    public static final String UOM_RATING_VALUE_REQUIRED = "UomRatingValue is required";

    public static final String USER_PREFERENCE_REQUIRED = "User Preference is required";
    public static final String USER_LOGIN_ID_REQUIRED = "User Login Id is required";
    public static final String USER_PREFERENCE_TYPE_ID_REQUIRED = "User Preference Type ID is required";

    public static final String CANNOT_UPDATE_USER = "Cannot update user";
    public static final String INVALID_DATE = "Invalid date";
    public static final String INVALID_DATE_ORDER = "Start date is not before end date";
    public static final String INVALID_EMAIL_ADDRESS = "Invalid email address";
    public static final String INVALID_POSTAL_ADDRESS = "Invalid postal address";
    public static final String INVALID_POSTAL_ADDRESS_CITY = "Invalid postal address city";
    public static final String INVALID_POSTAL_ADDRESS_MUNICIPALITY = "Invalid postal address municipality";
    public static final String INVALID_POSTAL_ADDRESS_PROVINCE = "Invalid postal address province";
    public static final String INVALID_POSTAL_ADDRESS_ZIP = "Invalid postal address ZIP code";
    public static final String INVALID_ROLE_NODE = "Each node should have at least an associate role";

    public static final String INVALID_CONSTRUCTION_SITE = "Invalid construction site";
    public static final String INVALID_YEAR = "Invalid year";
    public static final String INVALID_WEEK = "Invalid week";
    public static final String INVALID_USER_NODE = "User require one node";
    public static final String INVALID_FIRST_NAME = "Invalid first name";
    public static final String INVALID_ID = "Invalid identifier";
    public static final String INVALID_TARGET_KIND = "Invalid target";
    public static final String INVALID_CONTRACT_NUMBER = "Invalid contract number";
    public static final String INVALID_CONTRACT_AMOUNT = "Authorized amount cannot be greater than contract amount";
    public static final String INVALID_TAX_IDENTIFICATION_NUMBER = "Invalid Tax identification number";
    public static final String INVALID_VAT_NUMBER = "Invalid VAT Number";
    public static final String INVALID_SECONDMENT_FILTER = "Invalid secondment filter";
    public static final String INVALID_CODE = "Invalid code";
    public static final String INVALID_DESCRIPTION = "Invalid description";
    public static final String INVALID_LAST_NAME = "Invalid last name";
    public static final String INVALID_PAGE = "Invalid page value";
    public static final String INVALID_SIZE = "Invalid size value";
    public static final String INVALID_USERNAME = "Invalid username";
    public static final String INVALID_BUSINESS_NAME = "Invalid business name";
    public static final String INVALID_STATE = "Invalid state";
    public static final String INVALID_STATE_FOR_UPDATE = "Invalid state for update";
    public static final String INVALID_STATE_FOR_CHANGES = "Invalid state for changes";
    public static final String INVALID_REGISTRATION_NUMBER = "Invalid registration number";
    public static final String INVALID_REGISTRATION_NUMBER_ALREADY_EXIST = "Registration number already exists";
    public static final String INVALID_TYPE = "Invalid type";
    public static final String INVALID_CATEGORY = "Invalid category";
    public static final String INVALID_COMPANY = "Invalid company";
    public static final String INVALID_CONSTRUCTION_SITE_LOG = "Invalid construction site log id";
    public static final String INVALID_STOCK_CAPITAL = "Invalid stock capital";
    public static final String INVALID_CCIAA_DATE = "Invalid CCIAA certificate date";
    public static final String INVALID_ADDRESS = "Invalid address";
    public static final String INVALID_BIRTH_DATE = "Invalid birth date";
    public static final String INVALID_BIRTH_PLACE = "Invalid birth place";
    public static final String INVALID_BIRTH_LOCATION = "Invalid birth location";
    public static final String INVALID_GENDER = "Invalid gender";
    public static final String INVALID_RESIDENCY_PERMIT = "Invalid residency permit";
    public static final String INVALID_JOB_ORDER = "Invalid job order";
    public static final String INVALID_JOB_WITH_SAME_CODE_EXIST = "Job order with same code already exists";
    public static final String INVALID_EQUIPMENT = "Invalid equipment";
    public static final String INVALID_PERSON = "Invalid person";
    public static final String INVALID_START_DATE = "Invalid start date";
    public static final String INVALID_END_DATE = "Invalid end date";
    public static final String INVALID_ASSUMPTION_DATE = "Invalid assumption date";
    public static final String INVALID_BADGE = "Invalid badge";
    public static final String INVALID_LOT = "Invalid lot";
    public static final String INVALID_PREFECTURE = "Invalid prefecture";
    public static final String INVALID_CAUSAL = "Invalid causal";

    public static final String COMPANY_EMISSARY_REQUIRED = "An emissary company is required";
    public static final String COMPANY_REQUIRED = "An company is required";
    public static final String PERFORMING_COMPANY_REQUIRED = "A performing company is required";
    public static final String COMPANY_EMISSARY_UNIQUE = "Only one emissary company allowed";
    public static final String COMPANY_PERCENTAGE_NOT_ALLOWED = "Percentage must be greater than 0";
    public static final String COMPANY_PERCENTAGE_REQUIRED = "A percentage is required";
    public static final String COMPANY_INVALID_MAX_PERCENTAGE = "Total percentage must be less than or equals 100";
    public static final String COMPANY_TAX_OR_VAT_NUMBER_EXISTS = "The company's tax or VAT number has already been used";

    public static final String ATI_BUSINESS_NAME_EXISTS = "The ATI's name has already been used";

    public static final String USERNAME_ALREADY_EXIST = "Username already exists";


    public static final String INVALID_ATTACHMENT_TYPE = "Invalid attachment type";
    public static final String ATTACHMENT_NOT_FOUND = "Attachment not found";
    public static final String ATTACHMENT_ENTITY_NOT_FOUND = "Attachment entity not found";
    public static final String ATTACHMENT_SIZE_LIMIT_EXCEEDED = "Attachment size limit exceeded";
    public static final String ATTACHMENT_TYPE_NOT_FOUND = "Attachment type not found";
    public static final String CANNOT_UPDATE_ATTACHMENT_DESCRIPTION = "Cannot update attachment description";
    public static final String CANNOT_CREATE_ATTACHMENT = "Cannot create attachment";
    public static final String CANNOT_PUBLISH_MISSING_ATTACHMENTS = "Missing attachments";
    public static final String CANNOT_SAVE_ATTACHMENT = "Cannot archive attachment";
    public static final String CANNOT_DELETE_ATTACHMENT = "Cannot delete attachment";
    public static final String ATTACHMENT_ALREADY_EXIST = "Attachment already exists with different type";

    public static final String CANNOT_DOWNLOAD_ATTACHMENT = "Cannot download attachment for person";
    public static final String CANNOT_CREATE_TMP_FILE = "Cannot create temporary file";
    public static final String CANNOT_PRINT_PERSON_EMPLOYMENT_BADGE = "Cannot create person employment badge report";
    public static final String REPORT_TYPE_EQUIPMENT_NOT_FOUND = "Report type not found";

    public static final String CANNOT_UPDATE_POSTAL_ADDRESS = "Cannot update postal address";
    public static final String CANNOT_CREATE_POSTAL_ADDRESS = "Cannot create postal address";
    public static final String CANNOT_DELETE_POSTAL_ADDRESS = "Cannot delete postal address";

    public static final String CANNOT_UPDATE_COMPANY = "Cannot update company";
    public static final String CANNOT_CREATE_COMPANY = "Cannot create company";
    public static final String CANNOT_DELETE_COMPANY = "Cannot delete company";

    public static final String CANNOT_EXPORT_EXCEL = "Cannot export excel file for ";
    public static final String EXPORT_EXCEL_TYPE_NOT_FOUND = "Export excel type not found";

    public static final String CANNOT_UPDATE_ATI = "Cannot update ATI";
    public static final String CANNOT_CREATE_ATI = "Cannot create ATI";
    public static final String CANNOT_DELETE_ATI = "Cannot delete ATI";

    public static final String CANNOT_CREATE_EQUIPMENT = "Cannot create equipment";
    public static final String CANNOT_UPDATE_EQUIPMENT = "Cannot update equipment";
    public static final String CANNOT_DELETE_EQUIPMENT = "Cannot delete equipment";

    public static final String CANNOT_CREATE_EQUIPMENT_EMPLOYMENT = "Cannot create equipment employment";
    public static final String CANNOT_UPDATE_EQUIPMENT_EMPLOYMENT = "Cannot update equipment employment";
    public static final String CANNOT_DELETE_EQUIPMENT_EMPLOYMENT = "Cannot delete equipment employment";

    public static final String CANNOT_CREATE_PERSON = "Cannot create person";
    public static final String CANNOT_UPDATE_PERSON = "Cannot update person";
    public static final String CANNOT_DELETE_PERSON = "Cannot delete person";

    public static final String CANNOT_CREATE_PERSON_EMPLOYMENT = "Cannot create person employment";
    public static final String CANNOT_UPDATE_PERSON_EMPLOYMENT = "Cannot update person employment";
    public static final String CANNOT_DELETE_PERSON_EMPLOYMENT = "Cannot delete person employment";

    public static final String CANNOT_CREATE_JOB_ORDER = "Cannot create job order";
    public static final String CANNOT_UPDATE_JOB_ORDER = "Cannot update job order";
    public static final String CANNOT_DELETE_JOB_ORDER = "Cannot delete job order";

    public static final String CANNOT_CREATE_LOT = "Cannot create lot";
    public static final String CANNOT_UPDATE_LOT = "Cannot update lot";
    public static final String CANNOT_DELETE_LOT = "Cannot delete lot";

    public static final String CANNOT_CREATE_LOT_LOCATION = "Cannot create lot location";
    public static final String CANNOT_DELETE_LOT_LOCATION = "Cannot delete lot location";

    public static final String CANNOT_CREATE_CONSTRUCTION_SITE_LOCATION = "Cannot create construction site location";
    public static final String CANNOT_DELETE_CONSTRUCTION_SITE_LOCATION = "Cannot delete construction site location";

    public static final String CANNOT_UPDATE_WBS = "Cannot update WBS";
    public static final String CANNOT_CREATE_WBS = "Cannot create WBS";
    public static final String CANNOT_DELETE_WBS = "Cannot delete WBS";
    public static final String NOT_UNIQUE_WBS = "Wbs code must be unique for jobOrder";

    public static final String CANNOT_CREATE_CONTRACT = "Cannot create contract";
    public static final String CANNOT_UPDATE_CONTRACT = "Cannot update contract";
    public static final String CANNOT_DELETE_CONTRACT = "Cannot delete contract";

    public static final String CANNOT_CREATE_ANTIMAFIA_PROCESS = "Cannot create antimafia process";
    public static final String CANNOT_UPDATE_ANTIMAFIA_PROCESS = "Cannot update antimafia process";
    public static final String CANNOT_DELETE_ANTIMAFIA_PROCESS = "Cannot delete antimafia process";

    public static final String ANTIMAFIA_PROCESS_PHASE_NOT_FOUND = "Antimafia process not found";
    public static final String CANNOT_CREATE_ANTIMAFIA_PROCESS_PHASE = "Cannot create antimafia process phase";
    public static final String CANNOT_UPDATE_ANTIMAFIA_PROCESS_PHASE = "Cannot update antimafia process phase";
    public static final String CANNOT_DELETE_ANTIMAFIA_PROCESS_PHASE = "Cannot delete antimafia process phase";
    public static final String INVALID_ANTIMAFIA_PROCESS_PHASE_TYPE = "Invalid antimafia process phase type";
    public static final String NOT_UNIQUE_ANTIMAFIA_PROCESS_PHASE = "Antimafia process phase type must be unique for antimafia process";

    public static final String CANNOT_CREATE_WEEKLY_WORK_LOG = "Cannot create weekly work log";
    public static final String CANNOT_UPDATE_WEEKLY_WORK_LOG = "Cannot update weekly work log";
    public static final String CANNOT_DELETE_WEEKLY_WORK_LOG = "Cannot delete weekly work log";
    public static final String CANNOT_MARK_AS_COMPLETE_WEEKLY_WORK_LOG = "Cannot mark weekly work log as completed";
    public static final String WEEKLY_WORK_LOG_ALREADY_EXISTS = "Weekly work log already exists";

    public static final String CANNOT_CREATE_WORK_LOG = "Cannot create work log";
    public static final String CANNOT_UPDATE_WORK_LOG = "Cannot update work log";
    public static final String CANNOT_DELETE_WORK_LOG = "Cannot delete work log";
    public static final String CANNOT_MARK_AS_COMPLETE_WORK_LOG_INVALID_STATE = "Cannot mark work log as completed, it's in invalid state";
    public static final String CANNOT_MARK_AS_COMPLETE_WORK_LOG = "Cannot mark work log as completed";
    public static final String CANNOT_MARK_AS_COMPLETE_WORK_LOG_EVENTS_WITH_ERROR_STATE = "Cannot mark work log as completed because contains events with error state";
    public static final String CANNOT_DELETE_WORK_LOG_EVENTS = "Cannot delete work log because contains events";
    public static final String CANNOT_DELETE_WORK_LOG_EVENT = "Cannot delete work log because contains event";
    public static final String WORK_LOG_ALREADY_EXISTS = "Work log already exists";
    public static final String CANNOT_UPDATE_WORK_LOG_EVENT_COMPANY_NOT_AUTHORIZED = "Cannot save event because employment company is not authorized. Allowed states are 'Error' and 'Cancelled'";
    public static final String CANNOT_UPDATE_WORK_LOG_EVENT_EMPLOYMENT_NOT_AUTHORIZED = "Cannot save event because employment is not authorized. Allowed states are 'Error' and 'Cancelled'";
    public static final String CANNOT_UPDATE_WORK_LOG_EVENT_EMPLOYMENT_NOT_VALID = "Cannot save event because employment is valid for the date. Allowed states are 'Error' and 'Cancelled'";
    public static final String CANNOT_UPDATE_WORK_LOG_EVENT_PERSON_IS_INJURED = "Cannot save event because person is injured. Allowed states are 'Error' and 'Cancelled'";
    public static final String CANNOT_UPDATE_WORK_LOG_CONTAINS_EXTERNAL_EVENT = "Cannot update work log because contains external events";
    public static final String CANNOT_DELETE_WORK_LOG_CONTAINS_EXTERNAL_EVENT = "Cannot delete work log because contains external events";
    public static final String CANNOT_UPDATE_WORK_LOG_EVENTS_WBS_IS_NOT_EMPTY = "Cannot update events because wbs is not empty";
    public static final String CANNOT_UPDATE_DESTINATION_WORK_LOG_IS_COMPLETED = "Destination work log is completed";
    public static final String CANNOT_UPDATE_WORK_LOG_IS_COMPLETED = "Work log is completed";
    public static final String CANNOT_UPDATE_WORK_LOG_EVENTS_ALREADY_EXIST_ON_DESTINATION = "Cannot update, an events with same employment and timestamp exist in destination work log";
    public static final String CANNOT_APPLY_FILTER_WORK_LOG_IS_COMPLETED = "Cannot apply filter work log is completed";

    public static final String CANNOT_CREATE_CONSTRUCTION_SITE_LOG = "Cannot create construction site log";
    public static final String CANNOT_DELETE_CONSTRUCTION_SITE_LOG = "Cannot delete construction site log";
    public static final String CANNOT_UPDATE_CONSTRUCTION_SITE_LOG = "Cannot update construction site log";
    public static final String NOT_UNIQUE_CONSTRUCTION_SITE_LOG = "Construction site log logDate must be unique for constructionSite";
    public static final String CANNOT_CREATE_CONSTRUCTION_SITE_LOG_ACTIVITY = "Cannot create construction site log activity";
    public static final String CANNOT_UPDATE_CONSTRUCTION_SITE_LOG_ACTIVITY = "Cannot update construction site log activity";
    public static final String CANNOT_DELETE_CONSTRUCTION_SITE_LOG_ACTIVITY = "Cannot delete construction site log activity";
    public static final String CANNOT_UPDATE_CONSTRUCTION_SITE_LOG_MISSING_WORK_LOG = "cannot_update_construction_site_log_missing_work_log";
    public static final String CANNOT_UPDATE_CONSTRUCTION_SITE_LOG_WORK_LOG_IS_NOT_COMPLETED = "cannot_update_construction_site_log_work_log_is_not_completed";


    public static final String INVALID_AUTH_TYPE = "Invalid authentication type";
    public static final String MISSING_PASSWORD = "Password is missing";
    public static final String WRONG_PASSWORD = "Password does not match current one";
    public static final String SHORT_PASSWORD = "Password is too short";
    public static final String USER_NOT_FOUND = "User does not exists";

    public static final String TAG_IS_MANDATORY = "Tag is mandatory";
    public static final String ENTITY_STATE_IS_MANDATORY = "Entity state is mandatory";
    public static final String DEST_COMPANY_MUST_BE_DIFFERENT = "Destination company cannot be the same of assumption";
    public static final String START_DATE_BEFORE_END_DATE = "The start date must be before the end date";
    public static final String SIGNING_DATE_BEFORE_START_DATE = "Signing date must be before start date";
    public static final String SUBCONTRACT_DATE_BEFORE_START_DATE = "Subcontract date must be before start date";
    public static final String SIGNING_DATE_BEFORE_REVOCATION_DATE = "Signing date must be before revocation date";
    public static final String START_DATE_BEFORE_DISMISSAL_DATE = "The start date must be before the dismissal date";
    public static final String END_DATE_BEFORE_DISMISSAL_DATE = "The end date must be before the dismissal date";
    public static final String ASSUMPTION_DATE_BEFORE_START_DATE = "The assumption date must be before the start date";
    public static final String ASSUMPTION_DATE_BEFORE_DISMISSAL_DATE = "The assumption date must be before the dismissal date";
    public static final String TAX_IDENTIFICATION_NUMBER_EXISTS = "Tax identification number already present";
    public static final String EMPLOYMENT_DATES_OVERLAP = "Employment intervals cannot overlap";
    public static final String IMAGE_CANNOT_BE_RESIZED = "Image cannot be resized successfully";
    public static final String NOT_UNIQUE_EMAIL_ADDRESS = "User email must be unique";
    public static final String COMPANY_AND_PERFORMING_COMPANY_CANNOT_SAME = "Company and performing company cannot be equals";
    public static final String INVALID_CONTRACT_EXTENSION = "Invalid contract extension";

    public static final String INVALID_NOTIFICATION_EVENT_TYPE = "Invalid notification event type";
    public static final String INVALID_PERIOD = "Invalid period";
    public static final String INVALID_LOG_DATE = "Invalid logDate";

    public static final String COMMENT_ENTITY_NOT_FOUND = "Comment entity not found";
    public static final String INVALID_NOTE = "Invalid Note";
    public static final String INVALID_COMMENT_TYPE = "Invalid Comment Type";
    public static final String COMMENT_TYPE_EXISTS = "Comment Type already present";
    public static final String CANNOT_CREATE_COMMENT = "Cannot create comment";
    public static final String CANNOT_UPDATE_COMMENT = "Cannot update comment";
    public static final String CANNOT_DELETE_COMMENT = "Cannot delete comment";


    public static final String ACCIDENT_OVERLAP = "An accident already exist for selected period";
    public static final String CANNOT_DELETE_ACCIDENT = "Cannot delete accident";
    public static final String CANNOT_UPDATE_ACCIDENT = "Cannot update accident";
    public static final String CANNOT_CREATE_ACCIDENT = "Cannot create accident";
    public static final String INVALID_DURATION = "Invalid duration";
    public static final String INVALID_PERSON_EMPLOYMENT = "Invalid person employment";
    public static final String INVALID_EQUIPMENT_EMPLOYMENT = "Invalid equipment employment";

    public static final String INVALID_EXECUTION_INTERVAL = "Invalid execution interval";
    public static final String INVALID_NOTIFICATION_INTERVAL = "Invalid notification interval";

    public static final String INVALID_DIRECTION = "Invalid direction";
    public static final String INVALID_EVENT_TYPE = "Invalid event type";
    public static final String INVALID_EVENTS_LIST = "Events list is empty";
    public static final String INVALID_WORK_LOG = "Invalid work log";
    public static final String INVALID_REQUEST = "Invalid request";

    public static final String TIMESHEET_REQUIRED = "A timesheet is required";
    public static final String PARTY_ID_REQUIRED = "Party id is required";
    public static final String TIMESHEET_ID_REQUIRED = "Timesheet id is required";
    public static final String INVALID_TIMESHEET = "Invalid timesheet";
    public static final String TIME_ENTRY_ID_REQUIRED = "TimeEntry id is required";
    public static final String INVALID_TIME_ENTRY = "Invalid time entry";

    public static final String PERIOD_TYPE_REQUIRED = "PeriodType is required";
    public static final String PERIOD_TYPE_DESCRIPTION_REQUIRED = "PeriodType description is required";
    public static final String PERIOD_TYPE_ID_REQUIRED = "PeriodType id is required";
    public static final String INVALID_PERIOD_TYPE = "Invalid period type";

    public static final String CANNOT_SCHEDULE_PROBE = "Cannot schedule probe";
    public static final String CANNOT_UNSCHEDULE_PROBE ="Cannot unschedule probe" ;

    public static final String WORK_EFFORT_ASSOC_TYPE_ID_REQUIRED = "Work effort assoc type id is required";
    public static final String INVALID_WORK_EFFORT_ASSOC_TYPE = "Invalid work effort assoc type";
    public static final String WORK_EFFORT_ASSOC_TYPE_REQUIRED = "Work effort assoc type is required";
    public static final String WORK_EFFORT_ASSOC_TYPE_DESCRIPTION_REQUIRED = "Work effort assoc type description is required";

    public static final String WORK_EFFORT_SEQUENCE_REQUIRED = "Work effort sequence is required";
    public static final String WORK_EFFORT_SEQUENCE_NAME_REQUIRED = "Work effort sequence name is required";
    public static final String WORK_EFFORT_SEQUENCE_ID_REQUIRED = "Work effort sequence id is required";
    public static final String INVALID_WORK_EFFORT_SEQUENCE = "Invalid work effort sequence";

    public static final String DATA_SOURCE_TYPE_REQUIRED = "Data source type is required";
    public static final String DATA_SOURCE_TYPE_ID_REQUIRED = "Data source type id is required";
    public static final String DATA_SOURCE_TYPE_DESCRIPTION_REQUIRED = "Data source type description is required";
    public static final String INVALID_DATA_SOURCE_TYPE = "Invalid data resource type";

    public static final String DATA_RESOURCE_TYPE_REQUIRED = "Data resource type is required";
    public static final String DATA_RESOURCE_TYPE_ID_REQUIRED = "Data resource type id is required";
    public static final String DATA_RESOURCE_TYPE_DESCRIPTION_REQUIRED = "Data resource type description is required";
    public static final String INVALID_DATA_RESOURCE_TYPE = "Invalid data resource type";

    public static final String GL_FISCAL_TYPE_REQUIRED = "Gl fiscal type is required";
    public static final String GL_FISCAL_TYPE_ID_REQUIRED = "Gl fiscal type id is required";
    public static final String GL_FISCAL_TYPE_ENUM_ID_REQUIRED = "Gl fiscal type enum id is required";
    public static final String INVALID_GL_FISCAL_TYPE = "Invalid gl fiscal type";

    public static final String NOT_NULL = "Object cannot be null";
}
