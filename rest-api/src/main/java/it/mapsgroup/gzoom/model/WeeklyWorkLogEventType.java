package it.mapsgroup.gzoom.model;

/**
 * @author Andrea Fossi.
 */
public enum WeeklyWorkLogEventType {
    PERSON,
    PERSON_CHANGE_SET,
    EQUIPMENT,
    EQUIPMENT_CHANGE_SET
}
