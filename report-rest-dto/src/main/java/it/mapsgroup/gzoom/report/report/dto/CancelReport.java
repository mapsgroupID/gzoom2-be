package it.mapsgroup.gzoom.report.report.dto;

/**
 * @author Andrea Fossi.
 */
public class CancelReport {
    private String reason;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
