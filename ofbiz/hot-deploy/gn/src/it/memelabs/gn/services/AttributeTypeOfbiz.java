package it.memelabs.gn.services;

/**
 * 20/05/2014
 *
 * @author Andrea Fossi
 */
public enum AttributeTypeOfbiz {
    GN_ATTR_STRING,
    GN_ATTR_XML
}
