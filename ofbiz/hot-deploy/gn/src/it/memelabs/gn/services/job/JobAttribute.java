package it.memelabs.gn.services.job;

/**
 * 13/05/2014
 *
 * @author Andrea Fossi
 */
public enum JobAttribute {
    OWNER_NODE_ID,
    INVITATION_RECIPIENT_EMAILS,;
}
