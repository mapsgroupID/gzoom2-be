package it.memelabs.gn.services.security;

/**
 * 16/04/13
 *
 * @author Andrea Fossi
 */
public enum PermissionsOfbiz {
    GN_VIEW,
    GN_CREATE,
    GN_UPDATE,
    GN_DELETE,
    GN_ADMIN,
    GN_TREE_ADMIN,
    GN_NODE_ADMIN,
    GN_USER_ADMIN,
    GN_INVITE,
    GN_AUTH_VIEW,
    GN_AUTH_DRAFT,
    GN_AUTH_CANCEL,
    GN_AUTH_DELETE,
    //    GN_AUTH_WRITE,
    GN_AUTH_CONFIRM,
    GN_AUTH_PUBLISH,
    GN_AUTH_VALIDATE,
    GN_AUTH_CHECK,
    GN_AUTH_ROOT_VIEW,
    GN_COMPANY_ADMIN,
    GN_COMPANY_TREE_ADMIN,
    GN_COMPANY_WRITE,
    GN_COMPANY_BASE_WRITE,
    GN_AUDIT,
    GN_ARCHIVE,

    GN_AUTH_PVT_DRAFT,
    GN_AUTH_PVT_PUBLISH,
    GN_AUTH_PVT_CANCEL,
    //system user permission
    GN_AUTH_RECEIVE,
    GN_PARTY_INV_ADMIN,
    GN_SYSTEM_ADMIN,
    GN_COMM_ADMIN,   //communication Admin
    GN_UNIT_TEST


}
