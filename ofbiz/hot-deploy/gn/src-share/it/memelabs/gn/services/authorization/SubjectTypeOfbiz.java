package it.memelabs.gn.services.authorization;

/**
 * 26/03/13
 *
 * @author Andrea Fossi
 */
public enum SubjectTypeOfbiz {
    GN_ST_PRIVATE,
    GN_ST_COMPANY,
    GN_ST_MUNICIPALITY,
    GN_ST_RECYCLING_DEP,//GN_ST_RECYCLING_DEPOT
    GN_ST_WASTE_DUMP
}
