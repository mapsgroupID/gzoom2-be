package it.memelabs.gn.services.node;

/**
 * @author Elisa Spada
 */
public enum CompanyAttributeOfbiz {
    ROOT_PUBLISH,
    PRIVATE_AUTH_PUBLISH,
    PRIVATE_AUTH_CHECK,
    PASSWORD_CONSTRAINTS,
}