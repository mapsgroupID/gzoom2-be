package it.memelabs.gn.services.authorization;

/**
 * 26/04/13
 *
 * @author Andrea Fossi
 */
public enum  AuthorizationOriginOfbiz {
    GN_AUTH_ORG_HUMAN,
    GN_AUTH_ORG_APP,
    GN_AUTH_ORG_SCRAPING
}
