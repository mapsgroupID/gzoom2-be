package it.memelabs.gn.services.catalog;

/**
 * 23/04/13
 *
 * @author Andrea Fossi
 */
public enum OperationTypeOfbiz {
    DISPOSAL,
    RECYCLING

}
