package it.memelabs.gn.services.job;

/**
 * 05/05/2014
 *
 * @author Andrea Fossi
 */
public enum JobResultOfbiz {
    SUCCESS,
    FAILED,
}
