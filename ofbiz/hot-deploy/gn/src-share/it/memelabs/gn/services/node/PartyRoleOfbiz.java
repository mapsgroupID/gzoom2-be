package it.memelabs.gn.services.node;

/**
 * 26/04/13
 *
 * @author Andrea Fossi
 */
public enum  PartyRoleOfbiz {
    //todo: replace string ofbiz
    GN_CONTEXT,
    GN_USER,
    GN_COMPANY,
    GN_COMPANY_BASE,
    GN_ISSUER,
    GN_ISSUER_BASE,
    GN_ORGANIZATION_NODE,
    GN_STUB_NODE,
    GN_ROOT,
    GN_PVT_COMPANY,
    GN_PVT_COMPANY_BASE,
    GN_PROPAGATION_NODE,
    GN_NODE_CONTEXT,
    _NA_ // default empty role
}
