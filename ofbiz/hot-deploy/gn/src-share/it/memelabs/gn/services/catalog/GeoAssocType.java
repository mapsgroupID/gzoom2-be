package it.memelabs.gn.services.catalog;

/**
 * 24/05/13
 *
 * @author Andrea Fossi
 */
public enum GeoAssocType {
    POSTAL_CODE,
    REGIONS,
    GROUP_MEMBER,
    COUNTY_CITY,
    COUNTY_SEAT,

}
