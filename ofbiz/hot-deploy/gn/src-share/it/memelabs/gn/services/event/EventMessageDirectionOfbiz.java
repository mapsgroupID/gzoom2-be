package it.memelabs.gn.services.event;

/**
 * 31/03/14
 *
 * @author Elisa Spada
 */
public enum EventMessageDirectionOfbiz {
    UPWARD,
    DOWNWARD
}
