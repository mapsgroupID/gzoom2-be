package it.memelabs.gn.services.node;

/**
 * 22/04/14
 *
 * @author Elisa Spada
 */
public enum CompanyInvitationStateOfbiz {
    COMPANYINV_INVITED, //INVITED
    COMPANYINV_ACTIVE, //ACTIVE
    COMPANYINV_INACTIVE, //INACTIVE
}
