package it.memelabs.gn.services.job;

/**
 * 05/05/2014
 *
 * @author Andrea Fossi
 */
public enum JobStatusOfbiz {
    GN_ACT_JOB_SCHEDULED,
    GN_ACT_JOB_MISFIRE,
    GN_ACT_JOB_CANCELLED,
    GN_ACT_JOB_CONSUMED,
    GN_ACT_JOB_COMMITTED,
    GN_ACT_JOB_INCOMPLETE,//GN_ACT_JOB_INCOMPLETE
    GN_ACT_JOB_FAILED
}
