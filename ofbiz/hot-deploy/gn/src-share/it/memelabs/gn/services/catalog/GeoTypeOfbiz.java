package it.memelabs.gn.services.catalog;

/**
 * 20/05/13
 *
 * @author Andrea Fossi
 */
public enum GeoTypeOfbiz {
    MUNICIPALITY,
    PROVINCE,
    REGION,
    CITY,
    POSTAL_CODE,
    COUNTRY;

/*    GROUP,
    STATE,
    COUNTY,
    COUNTY_CITY,
    TERRITORY,
    SALES_TERRITORY,
    SERVICE_TERRITORY;*/
}
