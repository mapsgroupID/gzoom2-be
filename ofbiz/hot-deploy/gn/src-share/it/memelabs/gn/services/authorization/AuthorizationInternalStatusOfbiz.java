package it.memelabs.gn.services.authorization;

/**
 * 16/04/13
 *
 * @author Andrea Fossi
 */
public enum AuthorizationInternalStatusOfbiz {
    /**
     * GN_AUTH_TO_BE_DELETED
     */
    GN_AUTH_TO_BE_DELET,
    /**
     * GN_AUTH_TO_BE_RECEIVED
     */
    GN_AUTH_TO_BE_RECEI,
    /**
     * GN_AUTH_PEND_MERGE
     */
    GN_AUTH_PEND_MERGE,
}
