package it.memelabs.gn.services.login;

/**
 * 01/07/13
 *
 * @author Andrea Fossi
 */
public enum LoginSourceOfbiz {
    GN_LOG_SRC_INTERNAL,    //GN_LOGIN_SOURCE_INTERNAL
    GN_LOG_SRC_WEB_API,     //GN_LOGIN_SOURCE_WEB_API
    GN_LOG_SRC_WEB_UI,       //GN_LOGIN_SOURCE_WEB_UI
    GN_LOG_SRC_MOBILE       //GN_LOGIN_SOURCE_MOBILE
}
