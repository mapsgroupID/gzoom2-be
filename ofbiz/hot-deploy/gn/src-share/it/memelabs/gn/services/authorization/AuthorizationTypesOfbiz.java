package it.memelabs.gn.services.authorization;

/**
 * 16/04/13
 *
 * @author Andrea Fossi
 */
public enum AuthorizationTypesOfbiz {
    GN_AUTH_ALBO,
    //    GN_AUTH_PROVINCE,
    GN_AUTH_AIA,
    GN_AUTH_AUA,
    GN_AUTH_OTHER,
}
