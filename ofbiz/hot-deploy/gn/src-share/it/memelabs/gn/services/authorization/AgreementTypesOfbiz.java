package it.memelabs.gn.services.authorization;

/**
 * 16/04/13
 *
 * @author Andrea Fossi
 */
public enum AgreementTypesOfbiz {
    GN_AUTHORIZATION,  //GnAuthorization
    GN_AUTH_FILTER,    //GnAuthorizationFilter
    GN_AUTH_SLI_FILTER //GnAuthorizationSlicingFilter
}
