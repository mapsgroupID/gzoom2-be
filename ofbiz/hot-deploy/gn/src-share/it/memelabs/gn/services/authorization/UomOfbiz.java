package it.memelabs.gn.services.authorization;

/**
 * 27/03/13
 *
 * @author Andrea Fossi
 */
public enum UomOfbiz {
    WT_kg,   //Kg
    WT_mt,   //Tons
    VLIQ_L,  //Litres
    VDRY_m3, //Cubic meters
    LEN_m,   //meter
    LEN_km,  //Kilometer
    OTH_ea,  //Quantity
    TF_day,  //Day
    TF_wk,   //Week
    TF_mon,  //Month
    TF_yr    //Year
}
