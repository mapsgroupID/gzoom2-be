package it.memelabs.gn.services.communication;

/**
 * 30/05/13
 *
 * @author Andrea Fossi
 */
public enum CommunicationEventStatusOfbiz {
    COM_PENDING,
    COM_ENTERED,
    COM_IN_PROGRESS,
    COM_UNKNOWN_PARTY,
    COM_COMPLETE,
    COM_RESOLVED,
    COM_REFERRED,
    COM_BOUNCED,
    COM_CANCELLED,
    COM_NOT_DELIVERED,

}
