package it.memelabs.gn.services.authorization;

/**
 * 26/09/13
 *
 * @author Andrea Fossi
 */
public enum ResolveAuthorizationConflictActionOfbiz {
    GN_ACCEPT,
    GN_REJECT,
    GN_ACCEPT_DOCUMENTS
}
