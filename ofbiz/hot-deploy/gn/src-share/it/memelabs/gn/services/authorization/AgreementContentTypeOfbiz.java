package it.memelabs.gn.services.authorization;

/**
 * 26/06/13
 *
 * @author Andrea Fossi
 */
public enum AgreementContentTypeOfbiz {
    GN_AUTH_DOC, //GN_AUTHORIZATION_DOCUMENT
    GN_AUTH_SUPPL_DOC, //GN_AUTHORIZATION_SUPPLEMENTARY_DOCUMENT
    GN_AUTH_PAY_BRD_DOC //GN_AUTHORIZATION_PAYMENT_BURDEN_DOCUMENT
}
