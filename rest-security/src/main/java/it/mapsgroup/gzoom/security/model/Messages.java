package it.mapsgroup.gzoom.security.model;

/**
 * Security messages that are expected to be localized on the client side.
 *
 * @author Fabio G. Strozzi
 */
public class Messages {
    public static final String INVALID_USERNAME_OR_PASSWORD = "Invalid username or password";
    public static final String FORBIDDEN = "Forbidden";
    public static final String UNEXPECTED_ERROR = "Unexpected error occurred";
    public static final String NO_AUTHORIZATION_HEADER = "Authorization header not found";
    public static final String INVALID_AUTHORIZATION_SCHEMA = "Authorization schema not found";
    public static final String INVALID_AUTHORIZATION_TOKEN = "Invalid token";
    public static final String INVALID_ACCOUNT = "Invalid account";
    public static final String AD_LOGIN_FAILED = "Cannot login into Active Directory";
}
