package it.mapsgroup.gzoom.security.dto.models;

public class TokenDto {

    public final String token;

    public TokenDto(String token) {
        this.token = token;
    }

}