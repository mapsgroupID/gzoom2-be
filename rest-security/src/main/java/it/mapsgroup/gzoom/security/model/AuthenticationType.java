package it.mapsgroup.gzoom.security.model;

/**
 * @author Andrea Fossi.
 */
public enum AuthenticationType {
    LDAP, DB
}
