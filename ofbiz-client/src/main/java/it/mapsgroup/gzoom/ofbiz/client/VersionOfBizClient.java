package it.mapsgroup.gzoom.ofbiz.client;

import java.util.Map;

public interface VersionOfBizClient {

    Map<String, Object> getVersions();

}
