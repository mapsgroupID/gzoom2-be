package it.mapsgroup.gzoom.ofbiz.client;

public interface PingOfBizClient {

    /**
     * Sample ping method.
     *
     * @param message
     * @return
     */
    String ping(String message);

}