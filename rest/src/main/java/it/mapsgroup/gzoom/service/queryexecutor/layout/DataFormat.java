package it.mapsgroup.gzoom.service.queryexecutor.layout;

public enum DataFormat {
    A,D,T,I,N,N1,N2,P16,P32,L
}
