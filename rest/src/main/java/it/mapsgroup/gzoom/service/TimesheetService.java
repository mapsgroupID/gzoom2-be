package it.mapsgroup.gzoom.service;

import it.mapsgroup.gzoom.model.Messages;
import it.mapsgroup.gzoom.model.Result;
import it.mapsgroup.gzoom.model.Timesheet;
import it.mapsgroup.gzoom.querydsl.dao.TimesheetDao;
import it.mapsgroup.gzoom.querydsl.dto.TimesheetEx;
import it.mapsgroup.gzoom.querydsl.dto.WorkEffortTypeContentExt;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

import static it.mapsgroup.gzoom.security.Principals.principal;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * Profile service.
 *
 */
@Service
public class TimesheetService {
    private static final Logger LOG = getLogger(TimesheetService.class);

    private final TimesheetDao timesheetDao;
    private final DtoMapper dtoMapper;

    @Autowired
    public TimesheetService(TimesheetDao timesheetDao, DtoMapper dtoMapper) {
        this.timesheetDao = timesheetDao;
        this.dtoMapper = dtoMapper;
    }



    public Result<TimesheetEx> getTimesheet(String userLoginId, String context, String organization) throws SQLException {
        List<TimesheetEx> list = timesheetDao.getTimesheets(userLoginId, context, organization);
        return new Result<>(list, list.size());
    }

    public Result<WorkEffortTypeContentExt> getParamsTimesheet(String userLoginId, String context, String organization) {
        List<WorkEffortTypeContentExt> list = timesheetDao.getParamsTimesheets(userLoginId, context, organization);
        return new Result<>(list, list.size());
    }


    public String createTimesheet(Timesheet req) {
        Validators.assertNotNull(req, Messages.TIMESHEET_REQUIRED);
        Validators.assertNotBlank(req.getPartyId(), Messages.PARTY_ID_REQUIRED);
        it.mapsgroup.gzoom.querydsl.dto.Timesheet timesheet = new it.mapsgroup.gzoom.querydsl.dto.Timesheet();
        //timesheet.setTimesheetId(req.getTimesheetId());
        timesheet.setPartyId(req.getPartyId());
        timesheet.setFromDate(req.getFromDate().atStartOfDay());
        timesheet.setThruDate(req.getThruDate().atStartOfDay());
        timesheet.setActualHours(req.getActualHours());
        timesheet.setContractHours(req.getContractHours());
        timesheetDao.create(timesheet, principal().getUserLoginId());
        return timesheet.getTimesheetId();
    }

//    public String updateTimesheet(String id, Timesheet req) {
//        Validators.assertNotNull(req, Messages.TIMESHEET_REQUIRED);
//        Validators.assertNotBlank(req.getPartyId(), Messages.PARTY_ID_REQUIRED);
//
//        it.mapsgroup.gzoom.querydsl.dto.Timesheet record = timesheetDao.getTimesheet(id);
//        Validators.assertNotNull(record, Messages.INVALID_TIMESHEET);
//        copy(req, record);
//        timesheetDao.update(id, record, principal().getUserLoginId());
//        return req.getTimesheetId();
//    }

    public Boolean deleteTimesheet(String[] data) {
        for(String id : data ){
            Validators.assertNotBlank(id, Messages.TIMESHEET_ID_REQUIRED);
            it.mapsgroup.gzoom.querydsl.dto.Timesheet record = timesheetDao.getTimesheet(id);
            Validators.assertNotNull(record, Messages.INVALID_TIMESHEET);
            timesheetDao.delete(id);
        }
        return true;
    }

    public void copy( Timesheet from, it.mapsgroup.gzoom.querydsl.dto.Timesheet to) {
        to.setFromDate(from.getFromDate().atStartOfDay());
        to.setThruDate(from.getThruDate().atStartOfDay());
        to.setPartyId(from.getPartyId());
        to.setTimesheetId(from.getTimesheetId());
        to.setActualHours(from.getActualHours());
        to.setContractHours(from.getContractHours());
    }

}
