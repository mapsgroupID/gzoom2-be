package it.mapsgroup.gzoom.service.queryexecutor.layout;

public enum BorderStyle {
    THIN,
    MEDIUM,
    DOUBLE
}
