package it.mapsgroup.gzoom.service.queryexecutor.layout;

public enum StyleEnum {
    Width,
    BgColor,
    FontColor,
    FontSize,
    TextBold,
    TextHorizontalAlignment,
    TextVerticalAlignment,
    TextItalic,
    TextUnderline,
    CellsBorder,
    WrapText,
    DataFormat
}
