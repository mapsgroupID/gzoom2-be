package it.mapsgroup.gzoom.service;

import java.net.URL;

/**
 * OfBiz client configuration interface.
 *
 */
public interface GzoomReportClientConfig {

    /**
     * Gets the Report server URL.
     *
     * @return
     */
    URL getServerReportUrl();
}
