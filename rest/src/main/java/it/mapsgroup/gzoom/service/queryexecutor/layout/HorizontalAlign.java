package it.mapsgroup.gzoom.service.queryexecutor.layout;

public enum HorizontalAlign {
    L, R, C
}
