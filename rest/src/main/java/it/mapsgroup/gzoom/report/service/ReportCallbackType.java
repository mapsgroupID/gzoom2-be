package it.mapsgroup.gzoom.report.service;

/**
 * @author Andrea Fossi.
 */
public enum ReportCallbackType {
    TEST, REMINDER
}
