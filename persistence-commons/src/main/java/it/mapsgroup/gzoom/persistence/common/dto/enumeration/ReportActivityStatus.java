package it.mapsgroup.gzoom.persistence.common.dto.enumeration;

/**
 * @author Andrea Fossi.
 */
public enum ReportActivityStatus {
    QUEUED, RUNNING, DONE, FAILED, CANCELLED
}
