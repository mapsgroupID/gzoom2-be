package it.mapsgroup.gzoom.querydsl;

/**
 * @author Andrea Fossi.
 */
public interface AbstractIdentifiable extends AbstractIdentity {

	public String getDescription();
}
